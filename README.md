# <u>I</u>mage <u>A</u>nalysis <u>F</u>undamentals (iaf)

The `iaf` library is a helper library that adds user-friendly functionality to [scikit-image](https://scikit-image.org/),
[scipy](https://scipy.org/), [matplotlib](https://matplotlib.org/) and other libraries for the purpose of teaching the
fundamentals of image analysis.

## Installation

The `iaf` package installs all necessary dependencies. It is recommended to create a dedicated (conda or venv) 
environment for installation. 

```bash
$ conda create -n iaf-env python=3.11         # Optional
$ conda activate iaf-env                     # Optional
$ pip install --extra-index-url https://ia-res.ethz.ch/pypi iaf
```

Currently, [iaf](https://ia-res.ethz.ch/docs/iaf/index.html) is compatible with python 3.10, 3.11 and 3.12.

## Documentation

The official documentation is hosted on [https://ia-res.ethz.ch](https://ia-res.ethz.ch/docs/iaf/index.html). 