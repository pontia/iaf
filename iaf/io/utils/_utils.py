from pathlib import Path
from typing import Union

import requests


def download_file(
    url: str, target_dir: Union[Path, str] = None, local_filename: str = None
) -> Path:
    """Download a file from given URL to target folder and filename.

    Parameters
    ----------

    url: str
        URL of the file to dowload.

    target_dir: str
        Full path where to download the file. If omitted, it will fall back to current path.

    local_filename: str
        Local name of the downloaded file. If omitted, the file name will be extracted from the URL.

    Adapted from: https://stackoverflow.com/questions/16694907/download-large-file-in-python-with-requests.
    """
    if target_dir is None:
        target_dir = Path(".").absolute()
    Path(target_dir).mkdir(parents=True, exist_ok=True)
    if local_filename is None:
        local_filename = url.split("/")[-1]
    # NOTE the stream=True parameter below
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(Path(target_dir) / local_filename, "wb") as f:
            for chunk in r.iter_content(chunk_size=8192):
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                # if chunk:
                f.write(chunk)
    return Path(target_dir) / local_filename
