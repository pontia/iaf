from ._cast import safe_to_uint8, safe_to_uint16

__doc__ = "Data type casting utilities."
__all__ = ["safe_to_uint8", "safe_to_uint16"]
