import numpy as np


def safe_to_uint8(img: np.ndarray) -> np.ndarray:
    """Casts the input array to `np.uint8` making sure to avoid overflows. No rescaling is applied.

    Parameters
    ----------

    img: N-D array
        Two-dimensional, three-dimensional, or RGB(A) image of type `bool`, `np.uint16`, `np.float32`, `np.float64`
        to convert to `np.uint8`.

    Returns
    -------

    out: N-D array
        Same dimensionality of the input, but of type `np.uint8`.
    """

    if not isinstance(img, np.ndarray):
        raise ValueError("This function works on NumPy arrays only.")

    if img.dtype == bool:
        return img.astype(np.uint8)

    if img.dtype == np.uint8:
        return img

    if img.dtype == np.uint16:
        work = img.copy()
        work[work > 255] = 255
        return work.astype(np.uint8)

    if img.dtype in [np.float32, np.float64]:
        work = img.copy()
        work[work < 0] = 0
        work[work > 255] = 255
        return work.astype(np.uint8)

    raise ValueError("Unsupported input data type.")


def safe_to_uint16(img: np.ndarray) -> np.ndarray:
    """Casts the input array to `np.uint16` making sure to avoid overflows. No rescaling is applied.

    Parameters
    ----------

    img: N-D array
        Two-dimensional, three-dimensional, or RGB(A) image of type `bool`, `np.uint8`, `np.float32`, `np.float64`
        to convert to `np.uint16`.

    Returns
    -------

    out: N-D array
        Same dimensionality of the input, but of type `np.uint16`.
    """

    if not isinstance(img, np.ndarray):
        raise ValueError("This function works on NumPy arrays only.")

    if img.dtype == bool:
        return img.astype(np.uint16)

    if img.dtype == np.uint8:
        return img.astype(np.uint16)

    if img.dtype == np.uint16:
        return img

    if img.dtype in [np.float32, np.float64]:
        work = img.copy()
        work[work < 0] = 0
        work[work > 65535] = 65535
        return work.astype(np.uint16)

    raise ValueError("Unsupported input data type.")
