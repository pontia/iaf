from ._watershed import (
    estimate_object_sizes,
    filter_labels_by_area,
    get_label_areas,
    label_to_eroded_bw_mask,
    separate_neighboring_objects,
)

__doc__ = "Watershed-based operations."
__all__ = [
    "estimate_object_sizes",
    "filter_labels_by_area",
    "get_label_areas",
    "label_to_eroded_bw_mask",
    "separate_neighboring_objects",
]
