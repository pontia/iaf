from typing import Optional

import centrosome.cpmorphology
import centrosome.outline
import centrosome.propagate
import centrosome.threshold
import numpy as np
import scipy.ndimage
import scipy.sparse
import skimage.morphology
import skimage.segmentation
from skimage.measure import label, regionprops
from skimage.morphology import binary_erosion


def get_label_areas(label_image: np.ndarray) -> tuple:
    """Return a list of label areas from the label image.

    Parameters
    ----------

    label_image: numpy array
        Image labeled by scipy.ndimage.label

    Returns
    -------

    results: tuple
        Tuple with a list of label areas (as a NumPy array), the median area, and the median absolute deviation of areas.
    """
    props = regionprops(label_image)
    areas = []
    for prop in props:
        areas.append(prop.area)
    areas = np.array(areas)

    # Calculate median
    med_areas = np.median(areas)

    # Calculate median absolute deviation (in the same scale as the standard deviation)
    mad_areas = 1.4826 * np.median(np.abs(areas - med_areas))

    return areas, med_areas, mad_areas


def estimate_object_sizes(bw: np.ndarray):
    """Estimate area, min axis length, major axis length and equivalent diameter of all objects in a mask as their
    median values.

    Parameters
    ----------

    bw: numpy array
        Black and white mask.

    Returns
    -------

    results: tuple
        Tuple containing `(area, min_axis, max_axis, equiv_diam)` as the median values of the corresponding measurements
        for all objects.
    """

    # Count objects
    labels, num = label(bw, background=0, return_num=True, connectivity=1)

    # Get measurements
    props = regionprops(labels)

    areas = []
    min_axis = []
    max_axis = []
    equiv_diam = []
    for prop in props:
        areas.append(prop.area)
        min_axis.append(prop.axis_minor_length)
        max_axis.append(prop.axis_major_length)
        equiv_diam.append(prop.equivalent_diameter_area)
    area = np.median(np.array(areas))
    min_axis = np.median(np.array(min_axis))
    max_axis = np.median(np.array(max_axis))
    equiv_diam = np.median(np.array(equiv_diam))
    return area, min_axis, max_axis, equiv_diam


def filter_labels_by_area(
    label_image: np.ndarray, min_area: int, max_area: Optional[int] = None
) -> tuple:
    """Remove objects that have areas outside the specified range.

    Parameters
    ----------

    label_image: numpy array
        Image labeled by scipy.ndimage.label

    min_area: int
        Minimum allowed area of objects to be preserved.

    max_area: int
        Maximum allowed area of objects to be preserved. Optional, if omitted only small objects will be filtered.

    Returns
    -------

    results: Tuple
        The tuple contains the label image filtered by size, and the updated number of objects.
    """

    # Check that at least min_area is set
    if min_area is None:
        raise ValueError("Please provide a value for `min_area`.")

    # Do not modify the original
    work_label_image = label_image.copy()

    # Process the labels
    object_count = work_label_image.max()
    areas = np.array(
        scipy.ndimage.sum(
            np.ones(work_label_image.shape),
            work_label_image,
            np.array(list(range(0, object_count + 1)), dtype=np.int32),
        ),
        dtype=int,
    )
    area_image = areas[work_label_image]
    work_label_image[area_image < min_area] = 0
    if max_area is not None:
        work_label_image[area_image > max_area] = 0

    num = len(np.unique(work_label_image)) - 1
    return work_label_image, num


def label_to_eroded_bw_mask(nuclei_labels: np.ndarray, sel: np.array = np.ones((3, 3))):
    """Create a black-and-white mask from a label image. To keep the label
    separate in the back-and-white mask, they are individually eroded.

    Parameters
    ----------

    nuclei_labels: np.ndarray
        Label image.

    sel: np.ndarray
        Structuring element for erosion.

    Returns
    -------

    bw: np.ndarray
        Black-and-white mask.
    """
    out = 0 * nuclei_labels.copy()
    props = regionprops(nuclei_labels)
    for prop in props:
        min_row, min_col, max_row, max_col = prop.bbox
        mask = nuclei_labels[min_row:max_row, min_col:max_col]
        val = prop.label
        mask[mask != val] = 0
        mask = mask > 0
        mask = binary_erosion(mask, footprint=sel)
        mask = val * mask.astype(np.int32)
        out[min_row:max_row, min_col:max_col] = mask
    return out


def separate_neighboring_objects(
    bw_image: np.ndarray,
    label_image: np.ndarray,
    filter_size: Optional[int] = None,
    maxima_suppression_size: Optional[int] = None,
    unclump_method: Optional[str] = "shape",
    watershed_method: Optional[str] = "shape",
    fill_holes: str = "both",
    min_size: int = 20,
    max_size: int = 100,
    low_res_maxima: bool = True,
    exclude_border_objects: bool = False,
) -> tuple:
    """Separate objects based on intensity or distance transform.

    This is extracted, simplified and adapted from [CellProfiler](https://cellprofiler.org/)'s `IdentifyPrimaryObjects`
    module and uses a [modified](https://github.com/aarpon/centrosome) version of [centrosome](https://github.com/CellProfiler/centrosome).

    See: [IdentifyPrimaryObjects](https://github.com/CellProfiler/CellProfiler/blob/master/cellprofiler/modules/identifyprimaryobjects.py)'s
    source code

    Parameters
    ----------

    bw_image: numpy array
        Black-and-white binary mask.

    label_image: numpy array
        Image labeled by scipy.ndimage.label

    filter_size: int | None
        Filter size for image blurring (optional, default = None).
        If None, it will be calculated from range_min.

    maxima_suppression_size: int | None
        Size of mask for maximum suppression (optional, default = None)
        If None, it will be estimated automatically.

    unclump_method: str | None
        Method to unclump the objects. One of: { "intensity", "shape" }
            (Optional, default = "shape")

    watershed_method: str | None
        Method to unclump the objects. One of: { "intensity", "shape", "propagate" }
        (Optional, default = "intensity")

    fill_holes: str
        Whether and when to fill holes in the black-and-white mask. One of
            "never": holes are never filled
            "both": before and after declumping
            "after": after declumping only
        (Optional, default = "never")

    min_size: int
        Minimum expected diameter of objects.
        (Optional, default = 20)

    max_size: int
        Maximum expected diameter of objects.
        (Optional, default = 100)

    low_res_maxima: boolean
         Whether to down-sample the image for declumping. Will be ignored if `range_min` is < 10.
         (Optional, default = False)

    exclude_border_objects: boolean
         Whether to exclude objects touching the borders of the image (optional, default = False)

    Returns
    -------

    results: tuple
        Tuple with (label image: numpy array,  object_count: int, max_suppression_size: float)
    """

    if fill_holes not in ["never", "both", "after"]:
        raise ValueError("fill_holes must be one of ['never', 'both', 'after'].")

    automatic_suppression = False
    if maxima_suppression_size is None:
        automatic_suppression = True

    # Fill background holes inside foreground objects
    def size_fn(size, is_foreground):
        return size < max_size * max_size

    if fill_holes == "both":
        bw_image = centrosome.cpmorphology.fill_labeled_holes(bw_image, size_fn=size_fn)

    # If no filter is passed, calculate it
    if filter_size is None:
        filter_size = _calc_smoothing_filter_size(size_range_min=min_size)

    # Smooth mask
    blurred_image = _smooth_image(
        bw_image, np.ones(bw_image.shape, dtype=bool), filter_size
    )
    if min_size > 10 and low_res_maxima:
        image_resize_factor = 10.0 / float(min_size)
        if automatic_suppression:
            maxima_suppression_size = 7
        else:
            maxima_suppression_size = (
                maxima_suppression_size * image_resize_factor + 0.5
            )
        reported_maxima_suppression_size = maxima_suppression_size / image_resize_factor
    else:
        image_resize_factor = 1.0
        if automatic_suppression:
            maxima_suppression_size = min_size / 1.5
        else:
            maxima_suppression_size = maxima_suppression_size
        reported_maxima_suppression_size = maxima_suppression_size
    maxima_mask = centrosome.cpmorphology.strel_disk(
        max(1, maxima_suppression_size - 0.5)
    )
    distance_transformed_image = None
    if unclump_method == "intensity":
        # Remove dim maxima
        maxima_image = _get_maxima(
            blurred_image, label_image, maxima_mask, image_resize_factor
        )
    elif unclump_method == "shape":
        if fill_holes == "never":
            # For shape, even if the user doesn't want to fill holes,
            # a point far away from the edge might be near a hole.
            # So we fill just for this part.
            foreground = centrosome.cpmorphology.fill_labeled_holes(label_image) > 0
        else:
            foreground = label_image > 0
        distance_transformed_image = scipy.ndimage.distance_transform_edt(foreground)
        # randomize the distance slightly to get unique maxima
        np.random.seed(0)
        distance_transformed_image += np.random.uniform(
            0, 0.001, distance_transformed_image.shape
        )
        maxima_image = _get_maxima(
            distance_transformed_image,
            label_image,
            maxima_mask,
            image_resize_factor,
        )
    else:
        raise ValueError("Unsupported local maxima method: %s" % unclump_method)

    # Create the image for watershed
    if watershed_method == "intensity":
        # use the reverse of the image to get valleys at peaks
        watershed_image = 1 - bw_image
    elif watershed_method == "shape":
        if distance_transformed_image is None:
            distance_transformed_image = scipy.ndimage.distance_transform_edt(
                label_image > 0
            )
        watershed_image = -distance_transformed_image
        watershed_image = watershed_image - np.min(watershed_image)
    elif watershed_method == "propagate":
        # No image used
        pass
    else:
        raise NotImplementedError(
            "Watershed method %s is not implemented" % watershed_method
        )
    #
    # Create a marker array where the unlabeled image has a label of
    # -(nobjects+1)
    # and every local maximum has a unique label which will become
    # the object's label. The labels are negative because that
    # makes the watershed algorithm use FIFO for the pixels which
    # yields fair boundaries when markers compete for pixels.
    #
    labeled_maxima, object_count = scipy.ndimage.label(
        maxima_image, np.ones((3, 3), bool)
    )
    if watershed_method == "propagate":
        watershed_boundaries, distance = centrosome.propagate.propagate(
            np.zeros(labeled_maxima.shape),
            labeled_maxima,
            label_image != 0,
            1.0,
        )
    else:
        markers_dtype = np.int16 if object_count < np.iinfo(np.int16).max else np.int32
        markers = np.zeros(watershed_image.shape, markers_dtype)
        markers[labeled_maxima > 0] = -labeled_maxima[labeled_maxima > 0]

        #
        # Some labels have only one maker in them, some have multiple and
        # will be split up.
        #

        watershed_boundaries = skimage.segmentation.watershed(
            connectivity=np.ones((3, 3), bool),
            image=watershed_image,
            markers=markers,
            mask=label_image != 0,
        )

        watershed_boundaries = -watershed_boundaries

    # Now post-process
    # unedited_watershed_boundaries = watershed_boundaries.copy()

    # Filter out objects touching the border or mask
    border_excluded_watershed_boundaries = watershed_boundaries.copy()
    if exclude_border_objects:
        watershed_boundaries = _filter_on_border(bw_image, watershed_boundaries)
    border_excluded_watershed_boundaries[watershed_boundaries > 0] = 0

    # Filter out small and large objects
    exclude_objects_by_size = False  # Hard-coded to False because we provide different means to filter by area.
    if exclude_objects_by_size:
        size_excluded_watershed_boundaries = watershed_boundaries.copy()
        watershed_boundaries, removed_watershed_boundaries = _filter_on_size(
            watershed_boundaries, object_count, min_size, max_size
        )
        size_excluded_watershed_boundaries[watershed_boundaries > 0] = 0

    # Fill holes again after watershed
    if fill_holes != "never":
        watershed_boundaries = centrosome.cpmorphology.fill_labeled_holes(
            watershed_boundaries
        )

    # Relabel the image
    watershed_boundaries, object_count = centrosome.cpmorphology.relabel(
        watershed_boundaries
    )

    return watershed_boundaries, object_count, reported_maxima_suppression_size


def _smooth_image(image, mask, filter_size):
    """Apply the smoothing filter to the image"""

    if filter_size == 0:
        return image
    sigma = filter_size / 2.35
    #
    # We not only want to smooth using a Gaussian, but we want to limit
    # the spread of the smoothing to 2 SD, partly to make things happen
    # locally, partly to make things run faster, partly to try to match
    # the Matlab behavior.
    #
    filter_size = max(int(float(filter_size) / 2.0), 1)
    f = (
        1
        / np.sqrt(2.0 * np.pi)
        / sigma
        * np.exp(-0.5 * np.arange(-filter_size, filter_size + 1) ** 2 / sigma**2)
    )

    def fgaussian(image):
        output = scipy.ndimage.convolve1d(image, f, axis=0, mode="constant")
        return scipy.ndimage.convolve1d(output, f, axis=1, mode="constant")

    #
    # Use the trick where you similarly convolve an array of ones to find
    # out the edge effects, then divide to correct the edge effects
    #
    edge_array = fgaussian(mask.astype(float))
    masked_image = image.copy()
    masked_image[~mask] = 0
    smoothed_image = fgaussian(masked_image)
    masked_image[mask] = smoothed_image[mask] / edge_array[mask]
    return masked_image


def _calc_smoothing_filter_size(size_range_min=10):
    """Calculate the size of the smoothing filter from the min size of the objects."""
    return 2.35 * size_range_min / 3.5


def _get_maxima(image, labeled_image, maxima_mask, image_resize_factor):
    if image_resize_factor < 1.0:
        shape = np.array(image.shape) * image_resize_factor
        i_j = np.mgrid[0 : shape[0], 0 : shape[1]].astype(float) / image_resize_factor
        resized_image = scipy.ndimage.map_coordinates(image, i_j)
        resized_labels = scipy.ndimage.map_coordinates(
            labeled_image, i_j, order=0
        ).astype(labeled_image.dtype)

    else:
        resized_image = image
        resized_labels = labeled_image
    #
    # find local maxima
    #
    if maxima_mask is not None:
        binary_maxima_image = centrosome.cpmorphology.is_local_maximum(
            resized_image, resized_labels, maxima_mask
        )
        binary_maxima_image[resized_image <= 0] = 0
    else:
        binary_maxima_image = (resized_image > 0) & (labeled_image > 0)
    if image_resize_factor < 1.0:
        inverse_resize_factor = float(image.shape[0]) / float(
            binary_maxima_image.shape[0]
        )
        i_j = (
            np.mgrid[0 : image.shape[0], 0 : image.shape[1]].astype(float)
            / inverse_resize_factor
        )
        binary_maxima_image = (
            scipy.ndimage.map_coordinates(binary_maxima_image.astype(float), i_j) > 0.5
        )
        assert binary_maxima_image.shape[0] == image.shape[0]
        assert binary_maxima_image.shape[1] == image.shape[1]

    # Erode blobs of touching maxima to a single point

    shrunk_image = centrosome.cpmorphology.binary_shrink(binary_maxima_image)
    return shrunk_image


def _filter_on_border(image, labeled_image):
    """Filter out objects touching the border

    In addition, if the image has a mask, filter out objects
    touching the border of the mask.
    """
    border_labels = list(labeled_image[0, :])
    border_labels.extend(labeled_image[:, 0])
    border_labels.extend(labeled_image[labeled_image.shape[0] - 1, :])
    border_labels.extend(labeled_image[:, labeled_image.shape[1] - 1])
    border_labels = np.array(border_labels)
    #
    # the following histogram has a value > 0 for any object
    # with a border pixel
    #
    histogram = scipy.sparse.coo_matrix(
        (
            np.ones(border_labels.shape),
            (border_labels, np.zeros(border_labels.shape)),
        ),
        shape=(np.max(labeled_image) + 1, 1),
    ).todense()
    histogram = np.array(histogram).flatten()
    if any(histogram[1:] > 0):
        histogram_image = histogram[labeled_image]
        labeled_image[histogram_image > 0] = 0
    # elif image.has_mask:
    #     # The assumption here is that, if nothing touches the border,
    #     # the mask is a large, elliptical mask that tells you where the
    #     # well is. That's the way the old Matlab code works and it's duplicated here
    #     #
    #     # The operation below gets the mask pixels that are on the border of the mask
    #     # The erosion turns all pixels touching an edge to zero. The not of this
    #     # is the border + formerly masked-out pixels.
    #     mask_border = np.logical_not(
    #         scipy.ndimage.binary_erosion(image.mask)
    #     )
    #     mask_border = np.logical_and(mask_border, image.mask)
    #     border_labels = labeled_image[mask_border]
    #     border_labels = border_labels.flatten()
    #     histogram = scipy.sparse.coo_matrix(
    #         (
    #             np.ones(border_labels.shape),
    #             (border_labels, np.zeros(border_labels.shape)),
    #         ),
    #         shape=(np.max(labeled_image) + 1, 1),
    #     ).todense()
    #     histogram = np.array(histogram).flatten()
    #     if any(histogram[1:] > 0):
    #         histogram_image = histogram[labeled_image]
    #         labeled_image[histogram_image > 0] = 0
    return labeled_image


def _filter_on_size(labeled_image, object_count, range_min, range_max):
    """Filter the labeled image based on the size range

    labeled_image - pixel image labels
    object_count - # of objects in the labeled image
    returns the labeled image, and the labeled image with the
    small objects removed
    """
    if object_count > 0:
        areas = scipy.ndimage.sum(
            np.ones(labeled_image.shape),
            labeled_image,
            np.array(list(range(0, object_count + 1)), dtype=np.int32),
        )
        areas = np.array(areas, dtype=int)
        min_allowed_area = np.pi * (range_min * range_min) / 4
        max_allowed_area = np.pi * (range_max * range_max) / 4
        # area_image has the area of the object at every pixel within the object
        area_image = areas[labeled_image]
        labeled_image[area_image < min_allowed_area] = 0
        small_removed_labels = labeled_image.copy()
        labeled_image[area_image > max_allowed_area] = 0
    else:
        small_removed_labels = labeled_image.copy()
    return labeled_image, small_removed_labels
