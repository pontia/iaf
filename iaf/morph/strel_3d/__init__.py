from ._strel_3d import cube, ellipsoid, parallelepiped, sphere

__doc__ = "Three-dimensional structuring elements."
__all__ = ["sphere", "ellipsoid", "parallelepiped", "cube"]
