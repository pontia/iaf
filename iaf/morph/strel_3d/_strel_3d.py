from typing import Union

import numpy as np
from numpy import ndarray


def sphere(radius: Union[int, list[int]], dtype: np.dtype = np.uint8) -> ndarray:
    """Calculates a 3D structure element in the shape of a sphere.

    Parameters
    ----------

    radius: int
        Single positive integer.

    dtype: dtype
        Type of the structure element; optional, default is `np.uint8`.

    Returns
    -------

    str_el: ndarray (bool)
        3D structuring element.
    """
    return ellipsoid(radius, dtype)


def ellipsoid(radius: Union[int, list[int]], dtype: np.dtype = np.uint8) -> ndarray:
    """Calculates a 3D structure element in the shape of an ellipsoid (3D ellipse).

    Parameters
    ----------

    radius: Union[int, list[int]]
        Either a single positive integer, or a list of three positive integers `[x, y, z]`.

    dtype: dtype
        Type of the structure element; optional, default is `np.uint8`.

    Returns
    -------

    str_el: ndarray (bool)
        3D structuring element.
    """

    a = b = c = 0
    if type(radius) is int:
        a = b = c = radius
    elif (type(radius) is list or type(radius) is tuple) and len(radius) == 3:
        a = radius[0]
        b = radius[1]
        c = radius[2]
    else:
        ValueError("radius must be either a single integer or a list of three integers")

    if np.any(np.array([a, b, c]) < 1):
        ValueError("A radius must be a positive integer.")

    dc = 2 * c + 1
    db = 2 * b + 1
    da = 2 * a + 1

    # Since the ellipsoid is centro-symmetric, we only need to test one octant and
    # fill the other 7 symmetrically.
    M = np.zeros((dc, db, da), dtype=dtype)
    for z in range(-c, 1):
        for y in range(-b, 1):
            for x in range(-a, 1):
                # Test one octant
                if np.power(z / c, 2) + np.power(y / b, 2) + np.power(x / a, 2) <= 1:
                    zs = z + c
                    ys = y + b
                    xs = x + a
                    zm = dc - zs - 1
                    ym = db - ys - 1
                    xm = da - xs - 1

                    # Fill all 8 octants
                    M[zs, ys, xs] = 1
                    M[zs, ys, xm] = 1
                    M[zs, ym, xs] = 1
                    M[zs, ym, xm] = 1

                    M[zm, ys, xs] = 1
                    M[zm, ys, xm] = 1
                    M[zm, ym, xs] = 1
                    M[zm, ym, xm] = 1

    return M


def parallelepiped(side: Union[int, list[int]], dtype: np.dtype = np.uint8) -> ndarray:
    """Calculates a 3D structure element in the shape of a parallelepiped (3D rectangle).

    Parameters
    ----------

    side: Union[int, list[int]]
        Either a single positive integer, or a list of three positive integers `[x, y, z]`.

    dtype: dtype
        Type of the structure element; optional, default is `np.uint8`.

    Returns
    -------

    str_el: ndarray (bool)
        3D structuring element.

    """

    a = b = c = 0
    if type(side) is int:
        a = b = c = side
    elif (type(side) is list or type(side) is tuple) and len(side) == 3:
        a = side[0]
        b = side[1]
        c = side[2]
    else:
        ValueError("side must be either a single integer or a list of three integers")

    return np.ones((c, b, a), dtype=dtype)


def cube(side: int, dtype: np.dtype = np.uint8) -> ndarray:
    """Calculates a 3D structure element in the shape of a cube.

    Parameters
    ----------

    side: int
        Single positive integer.

    dtype: dtype
        Type of the structure element; optional, default is `np.uint8`.

    Returns
    -------

    str_el: ndarray (bool)
        3D structuring element.
    """

    return parallelepiped(side, dtype)
