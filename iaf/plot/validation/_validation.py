from pathlib import Path
from typing import Optional, Union

import matplotlib.pyplot as plt
import numpy as np


def plot_background_subtraction_control(
    img: np.ndarray,
    background: np.ndarray,
    img_corr: Optional[np.ndarray] = None,
    horizontal_profile_pos: Optional[int] = None,
    vertical_profile_pos: Optional[int] = None,
    figure_size: tuple = (12, 8),
    dpi: int = 150,
    out_file_name: Union[None, Path, str] = None,
) -> None:
    """Plot horizontal and vertical profiles across the image to display the result of background estimation.

    Parameters
    ----------

    img: np.ndarray
        Image before background subtraction.

    background: np.ndarray
        Image containing the background estimation.

    img_corr: np.ndarray | None
        Background-subtracted image. Omit to calculate from `img` and `background`.

    horizontal_profile_pos: Optional[int] = None
        Position of the horizontal profile to plot. Omit to default to the vertical center of the image.

    vertical_profile_pos: Optional[int] = None
        Position of the vertical profile to plot. Omit to default to the horizontal center of the image.

    figure_size: tuple (Optional)
        Size of the figure.

    dpi: int (Optional)
        Resolution of the figure (please notice, this is set whether the
        figure is saved or not).

    out_file_name: Union[None, Path, str] (Optional)
        Full file name to save the figure. The figure is only displayed
        if no file name is passed.
    """

    if img_corr is None:
        img_corr = img.astype(np.float32) - background.astype(np.float32)

    if horizontal_profile_pos is None:
        horizontal_profile_pos = img.shape[0] // 2

    if vertical_profile_pos is None:
        vertical_profile_pos = img.shape[1] // 2

    fig = plt.figure(constrained_layout=True, figsize=figure_size, dpi=dpi)
    spec = fig.add_gridspec(2, 6)
    ax01 = fig.add_subplot(spec[0, 0:2])
    ax02 = fig.add_subplot(spec[0, 2:4])
    ax03 = fig.add_subplot(spec[0, 4:6])

    ax10 = fig.add_subplot(spec[1, 0:3])
    ax11 = fig.add_subplot(spec[1, 3:6])

    # Show images
    ax01.imshow(img, cmap="gray")
    ax01.set_title("Original image")
    ax01.axis("off")

    ax02.imshow(background, cmap="gray")
    ax02.set_title("Background")
    ax02.axis("off")
    ax02.plot([0, img.shape[1]], [horizontal_profile_pos, horizontal_profile_pos], "r-")
    ax02.plot([vertical_profile_pos, vertical_profile_pos], [0, img.shape[0]], "r-")

    ax03.imshow(img_corr, cmap="gray")
    ax03.set_title("Result")
    ax03.axis("off")

    # Plot profiles
    profile_img_x = img[horizontal_profile_pos, :]
    profile_background_x = background[horizontal_profile_pos, :]
    profile_corr_x = img_corr[horizontal_profile_pos, :]
    ax10.plot(profile_img_x, label="img")
    ax10.plot(profile_background_x, label="bkgd")
    ax10.plot(profile_corr_x, label="corr")
    ax10.set_title("Horizontal profile")
    ax10.legend()

    profile_img_y = img[:, vertical_profile_pos]
    profile_background_y = background[:, vertical_profile_pos]
    profile_corr_y = img_corr[:, vertical_profile_pos]
    ax11.plot(profile_img_y, label="img")
    ax11.plot(profile_background_y, label="bkgd")
    ax11.plot(profile_corr_y, label="corr")
    ax11.set_title("Vertical profile")
    ax11.legend()

    # Save the figure if requested
    if out_file_name is not None:
        try:
            fig.savefig(str(out_file_name), dpi=dpi)
        except Exception:
            print(f"Error: could not save the figure to {out_file_name}!")
