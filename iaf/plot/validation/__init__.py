from ._validation import plot_background_subtraction_control

__doc__ = "Validation plotting functions."
__all__ = ["plot_background_subtraction_control"]
