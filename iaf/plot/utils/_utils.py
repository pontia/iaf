from pathlib import Path
from typing import Union

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm


def plot_parameter_search_1d(
    params: Union[np.ndarray, list],
    sses: Union[np.ndarray, list],
    p_best: float = None,
    sse_best: float = None,
    label_x: str = "Parameter",
    marker_size: int = 100,
    alpha: float = 0.75,
    figure_size: tuple = (12, 8),
    dpi: int = 150,
    out_file_name: Union[None, Path, str] = None,
) -> None:
    """Plot the SSE values and highlights the position of the minimum.

    Parameters
    ----------

    params: Union[np.ndarray, list]
        List or array of parameter values.

    sses: Union[np.ndarray, list]
        List or array of SSE values.

    p_best: float (Optional)
        Value of the parameter with the lowest SSE. If not specified, it will be searched.

    sse_best: float (Optional)
        Value of the lowest SSE. If not specified, it will be searched.

    label_x: str (Optional)
        Label for the x axis.

    marker_size: int (Optional)
        Size of the marker. By default, it is set to 100.

    alpha: float (Optional)
        Transparency (between 0.0 and 1.0) for the dots in the scatter plot.

    figure_size: tuple (Optional)
        Size of the figure.

    dpi: int (Optional)
        Resolution of the figure (please notice, this is set whether the
        figure is saved or not).

    out_file_name: Union[None, Path, str] (Optional)
        Full file name to save the figure. The figure is only displayed
        if no file name is passed.
    """

    # Make sure we are working with Numpy arrays
    params = np.asarray(params)
    sses = np.asarray(sses)

    # Create a figure
    fig, ax = plt.subplots(1, 1, figsize=figure_size, dpi=dpi)

    # Plot the data
    ax.scatter(params, sses, label="SSEs", s=marker_size, alpha=alpha)

    # If p_best and SSE_best are not specified, we get the from the data
    if sse_best is None:
        sse_best = sses.min()

    if p_best is None:
        p_best = params[np.argmin(sses)]

    # Plot (p_best, sse_best) on the same plot
    ax.scatter(p_best, sse_best, label="Best", s=1.5 * marker_size)

    # Set the limits
    range_x = params.max() - params.min()
    buffer_x = 0.025 * range_x
    ax.set_xlim((params.min() - buffer_x, params.max() + buffer_x))

    range_y = sses.max() - sses.min()
    buffer_y = 0.025 * range_y
    ax.set_ylim((sses.min() - buffer_y, sses.max() + buffer_y))

    # Set the axis labels
    ax.set_xlabel(label_x)
    ax.set_ylabel("SSE")

    # Show the grids
    ax.grid(True, which="major", color="gray", linestyle="-", linewidth=0.5)
    ax.minorticks_on()
    ax.grid(True, which="minor", color="gray", linestyle="--", linewidth=0.2)

    # Save the figure is requested
    if out_file_name is not None:
        try:
            fig.savefig(str(out_file_name), dpi=dpi)
        except Exception:
            print(f"Error: could not save the figure to {out_file_name}!")


def plot_parameter_search_2d(
    first_params: Union[np.ndarray, list],
    second_params: Union[np.ndarray, list],
    sses: np.ndarray,
    first_p_best: Union[np.ndarray, list, float] = None,
    second_p_best: Union[np.ndarray, list, float] = None,
    sse_best: Union[np.ndarray, list, float] = None,
    label_x: str = "First parameter",
    label_y: str = "Second parameter",
    marker_size: int = 200,
    show_colorbar: bool = False,
    axis_angle: Union[None, tuple] = None,
    figure_size: tuple = (16, 12),
    dpi: int = 150,
    out_file_name: Union[None, Path, str] = None,
) -> None:
    """Plot the SSE surface and highlights the position of the minimum.

    Parameters
    ----------

    first_params: Union[np.ndarray, list]
        List or array of the first parameter values.

    second_params: Union[np.ndarray, list]
        List or array of the second parameter values.

    sses: np.ndarray
        2-dimensional array of SSE values.

    first_p_best: Union[np.ndarray, list, float] (Optional)
        Value(s) of the first parameter with the lowest SSE. If not specified, it will be searched.

    second_p_best: Union[np.ndarray, list, float] (Optional)
        Value(s) of the second parameter with the lowest SSE. If not specified, it will be searched.

    sse_best: Union[np.ndarray, list, float] (Optional)
        Value(s) of the lowest SSE. If not specified, it will be searched.

    label_x: str (Optional)
        Label for the x axis.

    label_y: str (Optional)
        Label for the y axis.

    marker_size: int (Optional)
        Size of the marker. By default, it is set to 100.

    show_colorbar: bool (Optional)
        Set to True to display a colorbar.

    axis_angle: tuple (Optional)
        Pass a tuple with (elevation, azimuth) to rotate the axis.

    figure_size: tuple (Optional)
        Size of the figure.

    dpi: int (Optional)
        Resolution of the figure (please notice, this is set whether the
        figure is saved or not).

    out_file_name: Union[None, Path, str] (Optional)
        Full file name to save the figure. The figure is only displayed
        if no file name is passed.
    """

    # Make sure we are working with Numpy arrays
    first_params = np.asarray(first_params)
    second_params = np.asarray(second_params)
    sses = np.asarray(sses)
    if first_p_best is not None:
        first_p_best = np.atleast_1d(first_p_best)
    if second_p_best is not None:
        second_p_best = np.atleast_1d(second_p_best)
    if sse_best is not None:
        sse_best = np.atleast_1d(sse_best)

    # Prepare the mesh
    first_params_mg, second_params_mg = np.meshgrid(first_params, second_params)

    # Create a figure
    fig, ax = plt.subplots(
        subplot_kw={"projection": "3d"}, figsize=figure_size, dpi=dpi
    )

    # Plot the data as a 3D surface
    surf = ax.plot_surface(
        first_params_mg,
        second_params_mg,
        sses,
        cmap=cm.viridis,
        linewidth=0,
        antialiased=False,
    )

    # If p_best and SSE_best are not specified, we get the from the data
    if sse_best is None:
        sse_best = np.atleast_1d(sses.min())

    if first_p_best is None or second_p_best is None:
        first_p_best, second_p_best = np.where(sses == np.min(sses))

    # Plot (first_p_best, second_p_best, sse_best) on the same plot
    for f_indx, s_indx, b in zip(first_p_best, second_p_best, sse_best):
        f = first_params[f_indx]
        s = second_params[s_indx]
        ax.scatter(f, s, b, label="Best", s=1.5 * marker_size)

    # Set the limits
    range_x = first_params.max() - first_params.min()
    buffer_x = 0.025 * range_x
    ax.set_xlim((first_params.min() - buffer_x, first_params.max() + buffer_x))

    range_y = second_params.max() - second_params.min()
    buffer_y = 0.025 * range_y
    ax.set_ylim((second_params.min() - buffer_y, second_params.max() + buffer_y))

    range_z = sses.max() - sses.min()
    buffer_z = 0.025 * range_z
    ax.set_zlim((sses.min() - buffer_z, sses.max() + buffer_z))

    # Set the axis labels
    ax.set_xlabel(label_x)
    ax.set_ylabel(label_y)
    ax.set_zlabel("SSE")

    # Rotate the view
    if axis_angle is not None:
        ax.view_init(axis_angle[0], axis_angle[1])

    # ax.zaxis.set_major_locator(LinearLocator(10))
    # ax.zaxis.set_major_formatter('{x:.02f}')

    # Add a color bar which maps values to colors.
    if show_colorbar:
        fig.colorbar(surf, shrink=0.5, aspect=5)

    # Remove the border
    fig.tight_layout()

    # Save the figure is requested
    if out_file_name is not None:
        try:
            fig.savefig(str(out_file_name), dpi=dpi)
        except Exception:
            print(f"Error: could not save the figure to {out_file_name}!")
