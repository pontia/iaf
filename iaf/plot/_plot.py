import warnings
from pathlib import Path
from typing import Optional, Union

import matplotlib
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib.axes import Axes
from matplotlib.colors import ListedColormap
from skimage.measure import regionprops

from iaf.color import get_labels_cmap


def imshow(
    img: np.ndarray,
    *,
    cmap: Union[None, str, ListedColormap] = None,
    auto_stretch: bool = False,
    clip_percentile: float = 0.0,
    ax: Axes = None,
    title: Optional[str] = None,
    title_font_size: Optional[int] = 10,
) -> None:
    """Wrapper around matplotlib.pyplot.imshow that allows for toggling intensity stretching, hides axes, maximizes
    the canvas.

    Parameters
    ----------

    img: np.ndarray
        2D image to be displayed. The image can be gray-value (2D array) or RGB. If RGB, it must be in YXC or YXCA
        format.

    cmap: None|str|ListedColormap
        Color map to be used (optional). Only applies to gray-value images and is ignored for RGB images.

    auto_stretch: bool
        Whether to auto-stretch intensities for visualisation (optional, default = False).
        Please notice that this only applies to images of type `np.uint8` or `np.uint16`: `float` images will
        always be stretched.

    clip_percentile: float (Optional, default = 0.0)
        Percentile to clip intensity in the low and high parts of the dynamic range.
        Ignored if `auto_stretch` is `False`. As for `auto_stretch`, this only applies to images of type `np.uint8`
        and `np.uint16`.

    ax: matplotlib.axes.Axes (Optional, default = None)
        Axis handle. Pass a valid axes handle to display the image there; if omitted, a new figure and a new set of
        axes will be created.

    title: str
        Title for current axes (optional).

    title_font_size: int
        Font size for the title of current axes (optional).
    """

    # Color map
    if cmap is None:
        if img.ndim == 2:
            cmap = "gray"

    # Check image dimensionality
    if img.ndim == 3:
        if img.shape[2] not in [3, 4] and img.shape[0] not in [3, 4]:
            raise ValueError("Unsupported RGB image")
        if img.shape[0] in [3, 4]:
            img = np.swapaxes(img, 0, 2)

    # Set intensity limits and/or stretch if needed
    img, v_min, v_max = _stretch_and_convert(img, auto_stretch, clip_percentile)

    # Leave enough space for titles if ax is passed
    spacing_h = 0.1
    if ax is None:
        _, ax = plt.subplots(1, 1)
        spacing_h = 0.0
    ax.figure.subplots_adjust(
        top=1, bottom=0, right=1, left=0, hspace=spacing_h, wspace=0.0
    )
    plt.margins(0, 0)
    ax.set_axis_off()
    ax.xaxis.set_major_locator(plt.NullLocator())
    ax.yaxis.set_major_locator(plt.NullLocator())
    if title is not None and title != "":
        ax.set_title(title, fontsize=title_font_size)
    ax.imshow(img, cmap=cmap, vmin=v_min, vmax=v_max)


def show_labels(
    labels: np.ndarray,
    cmap=None,
    plot_centroids: bool = False,
    plot_labels: bool = False,
    ax: Axes = None,
    title: Optional[str] = None,
    title_font_size: Optional[float] = 10.0,
    label_font_size: Optional[Union[float, str]] = 10.0,
) -> None:
    """Plots a labels image with a suited color map by default.

    Parameters
    ----------

    labels: np.ndarray
        2D label image to be displayed. The image must be a label image.

    cmap: None|ListedColormap
        Color map to be used (optional). If omitted, a suitable one will be used.

    plot_centroids: bool
        Set to True to plot the centroids of the labels. Only one of `plot_centroids` and `plot_labels` can be `True`.

    plot_labels: bool
        Set to True to plot the label number on the object. Only one of `plot_centroids` and `plot_labels` can
        be `True`.

    ax: matplotlib.axes.Axes (Optional, default = None)
        Axis handle. Pass a valid axes handle to display the image there; if omitted, a new figure and a new set of
        axes will be created.

    title: str
        Title for current axes (optional).

    title_font_size: float
        Font size for the title of current axes (optional).

    label_font_size: float|str
        Font size for the labels. It can be either a number or one of {'xx-small', 'x-small', 'small', 'medium',
        'large', 'x-large', 'xx-large'}
    """

    if plot_centroids is True and plot_labels is True:
        raise ValueError("Only one of plot_centroids and plot_labels can be True.")

    if labels.ndim != 2:
        raise ValueError("A 2D label image is required.")

    if cmap is None:
        if labels.max() <= 255:
            cmap = get_labels_cmap(256)
        else:
            cmap = get_labels_cmap(65536)

    # Leave enough space for titles if ax is passed
    spacing_h = 0.1
    if ax is None:
        _, ax = plt.subplots(1, 1)
        spacing_h = 0.0
    ax.figure.subplots_adjust(
        top=1, bottom=0, right=1, left=0, hspace=spacing_h, wspace=0.0
    )
    plt.margins(0, 0)
    ax.set_axis_off()
    ax.xaxis.set_major_locator(plt.NullLocator())
    ax.yaxis.set_major_locator(plt.NullLocator())
    if title is not None and title != "":
        ax.set_title(title, fontsize=title_font_size)
    ax.imshow(labels, cmap=cmap, interpolation="nearest")
    if plot_centroids:
        props = regionprops(labels)
        for prop in props:
            ax.plot(prop.centroid[1], prop.centroid[0], "w*")
    if plot_labels:
        props = regionprops(labels)
        for prop in props:
            ax.text(
                prop.centroid[1] - 3,
                prop.centroid[0] + 3,
                str(prop.label),
                color="w",
                fontsize=label_font_size,
                fontweight="bold",
            )


def plot_data(
    x: Union[np.ndarray, list],
    y: Union[np.ndarray, list],
    data_name: str = "y",
    x2: Union[None, np.ndarray, list] = None,
    y_hat: Union[None, np.ndarray, list] = None,
    a: Union[None, float] = None,
    b: Union[None, float] = None,
    sse: Union[None, float] = None,
    model_for_legend: str = "linear",
    errors: Union[None, np.ndarray, list] = None,
    errors_label: Union[None, str] = None,
    legend_location: str = "best",
    label_x: str = "x",
    label_y: str = "y",
    lim_x: Union[None, tuple] = None,
    lim_y: Union[None, tuple] = None,
    marker_size: int = 100,
    alpha: float = 0.75,
    split_series: bool = True,
    figure_size: tuple = (12, 8),
    dpi: int = 150,
    out_file_name: Union[None, Path, str] = None,
) -> tuple:
    """Flexible plotting function for raw data (single or multiple series),
     fitted model (optional) and error bars (optional).

    Parameters
    ----------

    x: Union[np.ndarray, list]
        List or array of independent values `x`.

    y: Union[np.ndarray, list]
        List or array of dependent/target values `y`.

    data_name: str (Optional, default = "Data")
        Name of the data set to be displayed in the legend.

    x2: Union[None, np.ndarray, list] (Optional)
        List or array of independent values `x`; it is used to plot the predicted
        value `y_hat`. Omit if `x2` is the same as `x` (in the case `x` is `(m x n)`,
        `x2` will be `x[0, :]`). If specified, it must be a `(1 x n)` array.

    y_hat: Union[np.ndarray, list] (Optional)
        List or array of predicted values `y_hat`. If specified, it must be a `(1 x n)` array.

    a: Union[None, float] (Optional)
        Value of the slope of the predicted line (to be shown in the legend).

    b: Union[None, float] (Optional)
        Value of the intercept of the predicted line (to be shown in the legend).

    sse: Union[None, float] (Optional)
        Value of the sum of squared difference between `y` and `y_hat` (to be shown in the legend).

    alpha: float (Optional)
        Transparency (between 0.0 and 1.0) for the dots in the scatter plot.

    split_series: bool (Optional)
        Set to `True` (default) to display different series as separate scatter plots with
        own color, or to `False` to have them all in one plot (and one color).

    model_for_legend: str (Optional)
       Set the type of the model to be displayed (based on parameters `a` and `b`) in the legend.
       One of `"linear", "exp", "log", "sqrt"`; default is "linear".

    errors: Union[None, np.ndarray, list] (Optional)
        Errors to be plotted on the data. If specified, it must be a `(1 x n)` array.

    errors_label: Union[None, str] = None
        Name of the errors for the legend.

    legend_location: str (Optional)
        Location of the legend. By default, it is 'best'.
        See https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.legend.html

    label_x: str (Optional)
        Label of the x-axis (by default it is set to "x").

    label_y: str (Optional)
        Label of the y-axis (by default it is set to "y").

    lim_x: Union[None, tuple] (Optional)
        By default the extent of the data vector `x` defines the x-axis limit range.
        Explicitly set to a tuple `(x_min, x_max)` to override the x-axis limits.
        Either one of `x_min` or `x_max` can be None if current value should not be
        changed.
        In detail:
        If lim_x is None:
            The x-axis range will be defined by the limits of the data vector `x`.
        If lim_x is (None, None):
            The x-axis range will be defined by the limits of everything plotted.
        If lim_x is (0, None):
            The x-axis range will be 0 to the higher limit of everything plotted.
        If lim_x is (None, 100):
            The x-axis range will from the lower limit of everything plotted to 100.
        If lim_x is (0, 100):
            The x-axis range will go from 0 to 100.

    lim_y: Union[None, tuple] (Optional)
        By default the extent of the data vector/array `y` defines the y-axis
        limit range.
        Explicitly set to a tuple `(y_min, y_max)` to override the y-axis limits.
        Either one of y_min or y_max can be None if current value should not be
        changed.
        In detail:
        If lim_y is None:
            The y-axis range will be defined by the limits of the data vector/array `y`.
        If lim_y is (None, None):
            The y-axis range will be defined by the limits of everything plotted.
        If lim_y is (0, None):
            The y-axis range will be 0 to the higher limit of everything plotted.
        If lim_y is (None, 100):
            The y-axis range will from the lower limit of everything plotted to 100.
        If lim_y is (0, 100):
            The y-axis range will go from 0 to 100.

    marker_size: int (Optional)
        Size of the marker. By default, it is set to 100.

    split_series: bool (Optional)
        Set to `True` (default) to display different series as separate scatter plots with own color,
        or to `False` to have them all in one plot (and one color).

    figure_size: tuple (Optional)
        Size of the figure.

    dpi: int (Optional)
        Resolution of the figure (please notice, this is set whether the
        figure is saved or not).

    out_file_name: Union[None, Path, str] (Optional)
        Full file name to save the figure. The figure is only displayed
        if no file name is passed.

    Returns
    -------

    handles: tuple
        Tuple with current figure and axes.
    """

    # Make sure we are working with Numpy arrays
    x = np.atleast_1d(x)
    y = np.atleast_1d(y)

    if np.ndim(x) == 1:
        # Make sure to have a row vector
        x = np.reshape(x, (1, len(x)))

    if np.ndim(y) == 1:
        # Make sure to have a row vector
        y = np.reshape(y, (1, len(y)))

    # If y is a matrix and x is a vector, we transparently replicate
    # x to allow many-vs-one plots
    if np.ndim(y) == 2:
        if y.shape[1] != x.shape[1]:
            raise Exception("x and y must have the same number of elements!")
        if x.shape[0] == 1 and y.shape[0] > 1:
            x = np.repeat(x, y.shape[0], axis=0)

    # At this stage, x and y must have the same dimensions
    if y.shape[0] != x.shape[0] and y.shape[1] != x.shape[1]:
        raise Exception("x and y must have the same number of elements!")

    # Create a figure
    fig, ax = plt.subplots(1, 1, figsize=figure_size, dpi=dpi)

    # Plot the data

    if split_series and y.shape[0] > 1:
        colors = cm.plasma(np.linspace(0, 1, y.shape[0]))

        for i in range(y.shape[0]):
            ax.scatter(
                x[i, :],
                y[i, :],
                label=f"{data_name} #{i + 1}",
                color=colors[i, :],
                s=marker_size,
                alpha=alpha,
            )
    else:
        ax.scatter(x, y, label=data_name, s=marker_size, alpha=alpha)

    # Plot the fit
    if y_hat is not None:
        y_hat = np.atleast_1d(y_hat).squeeze()

        if y_hat.ndim > 1:
            raise ValueError("y_hat must be a (1 x n) array.")

        if x2 is None:
            if x.shape[0] > 1:
                x2 = x[0]
            else:
                x2 = x
        else:
            x2 = np.atleast_1d(x2).squeeze()
            if x2.ndim > 1:
                raise ValueError("x2 must be a (1 x n) array.")

        eq_str = ""
        sse_str = ""
        if a is not None and b is not None:
            if model_for_legend == "linear":
                if b >= 0:
                    eq_str = f"${a:.4f}\\cdot x + {b:.4f}$"
                else:
                    eq_str = f"${a:.4f}\\cdot x - {abs(b):.4f}$"
            elif model_for_legend == "exp":
                eq_str = f"${a:.4f}\\cdot \\exp({b:.4f}x)$"
            elif model_for_legend == "log":
                eq_str = f"${a:.4f} + {b:.4f}\\cdot \\ln(x)$"
            elif model_for_legend == "sqrt":
                eq_str = r"$<A> + \sqrt{<B>x}$"
                eq_str = eq_str.replace("<A>", f"{a:.4f}")
                eq_str = eq_str.replace("<B>", f"{b:.4f}")
            else:
                raise ValueError(
                    f"Bad value {model_for_legend} for `model_for_legend`."
                )
        if sse is not None:
            if eq_str != "":
                sse_str = f", SSE = {sse:.4f}"
            else:
                sse_str = f"SSE = {sse:.4f}"
        fit_name = f"{eq_str} {sse_str}"
        line = mlines.Line2D(x2, y_hat, color="red", linewidth=2, label=fit_name)
        # line.set_transform(ax.transAxes)
        ax.add_line(line)

    # If errors are specified, plot those as well
    if errors is not None:
        if errors_label is None:
            errors_label = "Errors"
        errors = np.atleast_1d(errors).squeeze()
        if errors.ndim > 1:
            raise ValueError("errors must be a (1 x n) array.")
        x_mean = x.mean(axis=0)
        y_mean = y.mean(axis=0)
        ax.errorbar(
            x_mean,
            y_mean,
            color="black",
            linewidth=0,
            markersize=10,
            yerr=errors,
            fmt="x",
            ecolor="black",
            elinewidth=3,
            label=errors_label,
        )

    # If needed, set the axis limits
    if lim_x is not None:
        curr_x_lim = ax.get_xlim()
        new_x_lim0 = lim_x[0] if lim_x[0] is not None else curr_x_lim[0]
        new_x_lim1 = lim_x[1] if lim_x[1] is not None else curr_x_lim[1]
        ax.set_xlim((new_x_lim0, new_x_lim1))
    else:
        range_x = x.max() - x.min()
        buffer_x = 0.025 * range_x
        ax.set_xlim((x.min() - buffer_x, x.max() + buffer_x))
    if lim_y is not None:
        curr_y_lim = ax.get_ylim()
        new_y_lim0 = lim_y[0] if lim_y[0] is not None else curr_y_lim[0]
        new_y_lim1 = lim_y[1] if lim_y[1] is not None else curr_y_lim[1]
        ax.set_ylim((new_y_lim0, new_y_lim1))
    else:
        range_y = y.max() - y.min()
        buffer_y = 0.025 * range_y
        ax.set_ylim((y.min() - buffer_y, y.max() + buffer_y))

    # Set the axis labels
    ax.set_xlabel(label_x)
    ax.set_ylabel(label_y)

    # Show the grids
    ax.grid(True, which="major", color="gray", linestyle="-", linewidth=0.5)
    ax.minorticks_on()
    ax.grid(True, which="minor", color="gray", linestyle="--", linewidth=0.2)

    # Add the legend
    ax.legend(loc=legend_location)

    # Save the figure if requested
    if out_file_name is not None:
        try:
            fig.savefig(
                str(out_file_name), bbox_inches="tight", pad_inches=0.1, dpi=dpi
            )
        except Exception:
            print(f"Error: could not save the figure to {out_file_name}!")

    return fig, ax


def add_fit(
    fig: matplotlib.figure.Figure,
    ax: matplotlib.axes.Axes,
    x: Union[np.ndarray, list],
    y_hat: Union[np.ndarray, list],
    a: Union[None, float] = None,
    b: Union[None, float] = None,
    sse: Union[None, float] = None,
    model_for_legend: str = "linear",
    legend_location: str = "best",
    dpi: int = 150,
    out_file_name: Union[None, Path, str] = None,
) -> tuple:
    """Add another fit to an existing plot_data() figure.

    Parameters
    ----------

    fig: Figure
        A matplotlib figure.

    ax: Union[np.ndarray, list]
        A matplotlib figure axis.

    x: Union[np.ndarray, list]
        List or array of independent values `x`.

    y_hat: Union[np.ndarray, list] (Optional)
        List or array of predicted values `y_hat`.

    a: Union[None, float] (Optional)
        Value of the slope of the predicted line (to be shown in the legend).

    b: Union[None, float] (Optional)
        Value of the intercept of the predicted line (to be shown in the legend).

    sse: Union[None, float] (Optional)
        Value of the sum of squared difference between `y` and `y_hat` (to be shown in the legend).

    model_for_legend: str (Optional)
        Set the type of the model to be displayed (based on parameters `a` and `b`) in the legend.
        One of `"linear", "exp", "log", "sqrt"`; default is "linear".

    legend_location: str (Optional)
        Location of the legend. By default, it is 'best'.
        See https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.legend.html

    dpi: int (Optional)
        Resolution of the figure (please notice, this is set whether the
        figure is saved or not).

    out_file_name: Union[None, Path, str] (Optional)
        Full file name to save the figure. The figure is only displayed
        if no file name is passed.

    Returns
    -------

    handles: tuple
        Tuple with current figure and axes.
    """

    # Make sure we are working with Numpy arrays
    x = np.atleast_1d(x)
    y_hat = np.atleast_1d(y_hat)

    # Raise an exception if we don't have a 1D vector x.
    if np.ndim(x) != 1:
        raise ValueError("'x' must be a vector.")

    # Raise an exception if we don't have a 1D vector y_hat.
    if np.ndim(y_hat) != 1:
        raise ValueError("'y_hat' must be a vector.")

    # Plot the fit
    if y_hat is not None:
        eq_str = ""
        sse_str = ""
        if a is not None and b is not None:
            if model_for_legend == "linear":
                if b >= 0:
                    eq_str = f"${a:.4f}\\cdot x + {b:.4f}$"
                else:
                    eq_str = f"${a:.4f}\\cdot x - {abs(b):.4f}$"
            elif model_for_legend == "exp":
                eq_str = f"${a:.4f}\\cdot \\exp({b:.4f}x)$"
            elif model_for_legend == "log":
                eq_str = f"${a:.4f} + {b:.4f}\\cdot \\ln(x)$"
            elif model_for_legend == "sqrt":
                eq_str = r"$<A> + \sqrt{<B>x}$"
                eq_str = eq_str.replace("<A>", f"{a:.4f}")
                eq_str = eq_str.replace("<B>", f"{b:.4f}")
            else:
                raise ValueError(
                    f"Bad value {model_for_legend} for `model_for_legend`."
                )
        if sse is not None:
            if eq_str != "":
                sse_str = f", SSE = {sse:.4f}"
            else:
                sse_str = f"SSE = {sse:.4f}"
        fit_name = f"{eq_str} {sse_str}"
        line = mlines.Line2D(x, y_hat, color="blue", linewidth=2, label=fit_name)
        # line.set_transform(ax.transAxes)
        ax.add_line(line)

    # Add the legend
    ax.legend(loc=legend_location)

    # Save the figure is requested
    if out_file_name is not None:
        try:
            fig.savefig(
                str(out_file_name), bbox_inches="tight", pad_inches=0.1, dpi=dpi
            )
        except Exception:
            print(f"Error: could not save the figure to {out_file_name}!")

    return fig, ax


# ======== Internal functions ========


def _stretch_and_convert(img, auto_stretch: bool = False, clip_percentile: float = 0.0):
    """Used by imshow: allows auto-stretching gray-value and RGB 8- and 16-bit images.

    Parameters
    ----------

    img: np.ndarray
        Image to be stretched and type converted.

    auto_stretch: bool
        Whether to auto-stretch intensities. This only applies to images of type `np.uint8` or `np.uint16`.
        Float images are left untouched.

    clip_percentile: float
        Percentile to clip intensity in the low and high parts of the dynamic range.
        Ignored if `auto_stretch` is `False`.

    Returns
    -------

    img: Optionally stretched and type-converted image.

    """

    # Make sure to work on a copy of the image
    work = img.copy()

    # Set intensity limits and/or stretch if needed
    if work.dtype in [np.uint8, np.uint16]:
        info = np.iinfo(work.dtype)
        v_min, v_max = info.min, info.max
        if auto_stretch:
            if clip_percentile == 0.0:
                v_min, v_max = work.min(), work.max()
            else:
                v_min = np.percentile(img, clip_percentile)
                v_max = np.percentile(img, 100.0 - clip_percentile)
            # Stretch and convert to 8 bit
            den = v_max - v_min
            if den > 0:
                work_norm = (work.astype(np.float32) - v_min) / den
            else:
                # This can happen if the result of clipping is a 0-image
                warnings.warn(
                    f"Clipping at ({clip_percentile}, {100.0 - clip_percentile}) "
                    f"would result in a zero image. Skipping..."
                )
                work_norm = (work.astype(np.float32) - info.min) / info.max
            work_norm = np.clip(work_norm, 0.0, 1.0)
            work = (255.0 * work_norm).astype(np.uint8)
            v_min, v_max = 0, 255
        else:
            if work.dtype == np.uint16:
                # Convert to 8 bit to make plt.imshow() render something usable
                work = (255.0 * (work.astype(np.float32) / 65535)).astype(np.uint8)
                v_min, v_max = 0, 255
    else:
        v_min = None
        v_max = None

    return work, v_min, v_max
