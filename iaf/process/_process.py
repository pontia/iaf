import time
from typing import Optional, Tuple, Union

import cv2
import numpy as np
from skimage.filters import gaussian
from skimage.morphology import disk
from skimage.restoration import ellipsoid_kernel, rolling_ball
from skimage.transform import resize


def subtract_background(
    img: np.ndarray,
    algorithm: str = "rolling_ball",
    radius: int = 25,
    offset: int = 0,
    down_size_factor: int = 1,
    return_background: bool = False,
) -> Union[np.ndarray, tuple[np.ndarray, np.ndarray]]:
    """Runs a background subtraction on the given image using the specified algorithm.

    See
    ___

    * https://scikit-image.org/docs/dev/api/skimage.restoration.html#rolling-ball
    * https://scikit-image.org/docs/dev/api/skimage.morphology.html#opening
    * https://scikit-image.org/docs/dev/api/skimage.filters.html#gaussian

    Parameters
    ----------

    img: np.ndarray
        Image to be processed

    algorithm : str
        Algorithm to be used for the background subtraction.
        One of `{"rolling ball", "morphological_opening", "gaussian"}`
        (Optional, default = `"rolling_ball"`)

    radius: int
        Radius to be used for the morphological opening structural element, the rolling ball, or the Gaussian kernel.
        (Optional, default = 25)

        The value of `radius` will be automatically scaled if a `down_size_factor` != 1.0 is set.

        Please also notice: for better results with the Gaussian kernel approach, the sigma of the kernel is set to
        `radius / sqrt(2)`.

    offset: int
        Offset to be used to increase or decrease the intensities of the estimated background.
        (Optional, default = 0)

    down_size_factor: int
        Tune the accuracy of background estimation by optionally down-sampling the image. The final result will
        be full sized, no matter the value of `down_size_factor`. The default `down_size_factor` value of 1 means
        that the background estimation is performed on the original image; a value of 2 means rescaling the image
        by a factor 1/2 in both x and y directions (that is, a 4x smaller image); a value of 4 rescales in x and y
        directions by a factor of 1/4 (that is, a 16x smaller image); and so on.

    return_background: bool
        Whether the estimated background should be returned along with the background-subtracted image.
        (Optional, default = `False`)

    Returns
    -------
    corr | (corr, background): Union[np.ndarray, tuple[np.ndarray, np.ndarray]]
        Single np.ndarray, if `return_background = False`, or a tuple with two np.ndarrays: background-subtracted image
        and estimated background.
    """

    # This commodity function only works on single plane, one-channel images
    if img.ndim != 2:
        raise ValueError(
            "The image to process must be a single-plane, one-channel image."
        )

    # Convert the image to float and remember the original data type
    original_dtype = img.dtype
    img = img.astype(np.float32)

    # If requested, scale image and radius (also consider a value of 0 to mean no rescaling)
    if down_size_factor == 0 or down_size_factor == 1:
        work_img = img.copy()
        work_radius = radius
    else:
        work_img = resize(
            img,
            (img.shape[0] // down_size_factor, img.shape[1] // down_size_factor),
            anti_aliasing=True,
        )
        work_radius = radius // down_size_factor

    # Make sure the radius is odd
    if work_radius % 2 == 0 or work_radius < 1:
        work_radius += 1

    # Make sure to have a lowercase string
    algorithm = str(algorithm).lower()

    # Estimate the background
    if algorithm == "morphological_opening":
        str_el = disk(work_radius)
        background = cv2.morphologyEx(work_img, cv2.MORPH_OPEN, str_el) + offset

    elif algorithm == "rolling_ball":
        kernel = ellipsoid_kernel((work_radius * 2, work_radius * 2), work_radius * 2)
        background = rolling_ball(work_img, kernel=kernel) + offset

    elif algorithm == "gaussian":
        # From experiments, we tweak sigma a bit to get better results
        sigma = int(np.round(work_radius / np.sqrt(2)))
        if sigma % 2 == 0:
            sigma += 1
        background = gaussian(work_img, sigma=sigma, preserve_range=True) + offset

    else:
        raise ValueError(f"Unknown algorithm={algorithm}.")

    # If needed, scale the background up to match the size of the original image
    if down_size_factor != 0 and down_size_factor != 1:
        background = resize(
            background, (img.shape[0], img.shape[1]), anti_aliasing=True
        )

    # Subtract the background
    corr = img - background

    # In case of integer types, check if clipping is needed
    if np.issubdtype(original_dtype, np.integer):
        # Get min and max values for type
        min_value = np.iinfo(original_dtype).min
        max_value = np.iinfo(original_dtype).max

        # Set values outside of the range to the bounds
        corr[corr < min_value] = min_value
        corr[corr > max_value] = max_value

    # Convert back to the original data  type
    corr = corr.astype(original_dtype)
    background = background.astype(original_dtype)

    if return_background:
        # Return the background-subtracted image and the background estimation
        return corr, background

    else:
        # Return the background-subtracted image
        return corr


def tile(
    image: np.ndarray,
    tile_size: tuple,
    overlap_pixels: Optional[int] = None,
    overlap_percent: Optional[float] = None,
    drop_partial: bool = True,
) -> Tuple[list, int, int]:
    """Breaks a 2D images into a series of tiles of given size and optional overlap.

    Parameters
    ----------

    image: numpy array
        Original intensity image.

    tile_size: tuple
        Size (y, x) of each of the tiles.

    overlap_pixels: Optional[int]
        Size in pixels of the tile overlapping area.

    overlap_percent: Optional[float]
        Size in percent of the tile overlapping area.

        If both `overlap` and `overlap_percent` are defined, the value of `overlap` will be used.

    drop_partial: bool
        Whether tiles at the borders that are smaller than `tile_size` should be dropped.

    Returns
    -------
    tile_list: List
        List of tiles in row-first order, each of which is an np.ndarray.

    n_rows: int
        Number of rows

    n_cols: int
        Number of columns
    """

    # Check inputs
    if image.ndim != 2:
        raise ValueError("The image must be 2D.")

    if len(tile_size) != 2 or tile_size[0] <= 1 or tile_size[1] <= 1:
        raise ValueError(
            "`tile_size` must be a tuple with two positive, integer values."
        )

    if overlap_pixels is not None and (
        overlap_pixels < 0 or overlap_pixels >= min(tile_size)
    ):
        raise ValueError(
            "`overlap_pixels` must be a number >= 0 and smaller than the minimum tile size."
        )

    if overlap_percent is not None and (
        overlap_percent < 0.0 or overlap_percent >= 1.0
    ):
        raise ValueError("`overlap_percent` must be a number >= 0.0 and < 1.0.")

    # Make sure the tile is not larger than the image
    val_tile_size_y = int(tile_size[0])
    val_tile_size_x = int(tile_size[1])
    if tile_size[0] > image.shape[0]:
        val_tile_size_y = image.shape[0]
    if tile_size[1] > image.shape[1]:
        val_tile_size_x = image.shape[1]
    tile_size = (val_tile_size_y, val_tile_size_x)

    # Define the overlap
    overlap = 0
    if overlap_pixels is None:
        overlap_pixels = 0
    if overlap_percent is None:
        overlap_percent = 0.0
    if overlap_pixels == 0 and overlap_percent == 0.0:
        overlap = 0
    elif overlap_pixels >= 0 and overlap_percent == 0.0:
        overlap = overlap_pixels
    elif overlap_pixels == 0 and overlap_percent >= 0.0:
        overlap = int(overlap_percent * float(tile_size[0]))
    elif overlap_pixels >= 0 and overlap_percent >= 0.0:
        overlap = overlap_pixels

    # Calculate bounds
    dy = tile_size[0] - overlap
    dx = tile_size[1] - overlap
    start_y = np.arange(0, image.shape[0], dy)
    end_y = start_y + tile_size[0]
    if end_y[-1] > image.shape[0]:
        end_y[-1] = image.shape[0]
    start_x = np.arange(0, image.shape[1], dx)
    end_x = start_x + tile_size[1]
    if end_x[-1] > image.shape[1]:
        end_x[-1] = image.shape[1]

    assert len(start_y) == len(end_y), "Problems with calculating tile boundaries!"
    assert len(start_x) == len(end_x), "Problems with calculating tile boundaries!"

    # Should we drop tiles at the edges if they are smaller than the requested size?
    if drop_partial and len(end_y) > 1 and (end_y[-1] - end_y[-2]) < tile_size[0]:
        start_y = start_y[:-1]
        end_y = end_y[:-1]
    if drop_partial and len(end_x) > 1 and (end_x[-1] - end_x[-2]) < tile_size[1]:
        start_x = start_x[:-1]
        end_x = end_x[:-1]

    tile_list = []
    for i in range(len(start_y)):
        y0 = start_y[i]
        y = end_y[i]
        for j in range(len(start_x)):
            x0 = start_x[j]
            x = end_x[j]
            tile_list.append(image[y0:y, x0:x])

    # Number of rows and columns
    n_rows = len(start_y)
    n_cols = len(start_x)

    return tile_list, n_rows, n_cols


def sample(image: np.ndarray, size: tuple, seed: Optional[int] = None) -> np.ndarray:
    """Returns a random subset of given shape from the passed 2D image.

    Parameters
    ----------

    image: numpy array
        Original intensity image.

    size: tuple
        Size (y, x) of the subset of the image to be randomly extracted.

    seed: Optional[int]
        Random generator seed to reproduce the sampling. Omit to create a
        new random sample every time.

    Returns
    -------

    subset: np.ndarray
        Subset of the image of given size.
    """

    if image.ndim != 2:
        raise ValueError("The image must be 2D.")

    # Initialize random-number generator
    if seed is None:
        seed = time.time_ns()
    rng = np.random.default_rng(seed)

    # Get starting point
    max_y = image.shape[0] - size[0]
    max_x = image.shape[1] - size[1]
    y0 = int(rng.uniform(0, max_y))
    x0 = int(rng.uniform(0, max_x))

    # Return the subset
    return image[y0 : y0 + size[0], x0 : x0 + size[1]]
