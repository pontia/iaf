import numpy as np
from scipy.ndimage import uniform_filter


def std_filter(img: np.ndarray, support: int = 3) -> np.ndarray:
    """Perform 2D standard-deviation filtering.

    Parameters
    ----------

    img: np.ndarray
        Image to be processed

    support: int
        Defines the kernel size ([support, support])
        (Optional, default = 3)

    Returns
    -------

    filt: np.ndarray
        Filtered image.
    """

    return np.sqrt(var_filter(img, support))


def var_filter(img: np.ndarray, support: int = 3) -> np.ndarray:
    """Perform 2D variance filtering.

    Parameters
    ----------

    img: np.ndarray
        Image to be processed

    support: int
        Defines the kernel size ([support, support])
        (Optional, default = 3)

    Returns
    -------

    filt: np.ndarray
        Filtered image.
    """

    if support <= 1:
        raise ValueError("Support must be greater than 1.")

    img = img.astype(float)
    c1 = uniform_filter(img, support, mode="mirror")
    c2 = uniform_filter(img * img, support, mode="mirror")
    var = c2 - c1 * c1
    var[var < 0] = 0  # Rounding error can be negative
    return var
