from ._filter import std_filter, var_filter

__doc__ = "Filters and functions."
__all__ = ["std_filter", "var_filter"]
