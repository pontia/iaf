from ._stats import hist_bins, ideal_hist_bins, prepare_histogram

__doc__ = "Statistics functionalities."
__all__ = [
    "hist_bins",
    "ideal_hist_bins",
    "prepare_histogram",
]
