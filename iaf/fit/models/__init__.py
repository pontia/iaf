from ._models import (
    exponential_model,
    linear_model,
    logarithmic_model,
    square_root_model,
)

__doc__ = "Implementation of simple models."
__all__ = [
    "linear_model",
    "exponential_model",
    "logarithmic_model",
    "square_root_model",
]
