import numpy as np


def linear_model(x: np.ndarray, a: float, b: float):
    """Linear model `y_hat = a * x + b`.

    Parameters
    ----------

    x : np.ndarray
        Independent variable.

    a: float
        Slope of the line `ax + b`.

    b: float
        Intercept of the line `ax + b`.

    Returns
    -------

    y_hat: np.ndarray
        Predicted `y` values.
    """
    if type(x) is list:
        x = np.array(x)
    y_hat = a * x + b
    return y_hat


def exponential_model(x: np.ndarray, a: float, b: float):
    """Exponential model `y_hat = a * exp(b * x)`.

    Parameters
    ----------

    x : np.ndarray
        Independent variable.

    a: float
        Scaling parameter of the exponential `a * exp(b * x)`.

    b: float
        Scaling parameter of independent variable in the exponential `a * exp(b * x)`.

    Returns
    -------

    y_hat: np.ndarray
        Predicted `y` values.
    """
    y_hat = a * np.exp(b * x)
    return y_hat


def logarithmic_model(x: np.ndarray, a: float, b: float):
    """Logarithmic model `y_hat = a + b * ln(x)`.

    Parameters
    ----------

    x : np.ndarray
        Independent variable.

    a: float
        Intercept value of the logarithmic model `a + b * ln(x)`.

    b: float
        Scaling parameter of independent variable in the logarithmic model `a + b * ln(x)`.

    Returns
    -------

    y_hat: np.ndarray
        Predicted `y` values.
    """
    y_hat = a + b * np.log(x)
    return y_hat


def square_root_model(x: np.ndarray, a: float, b: float):
    """Square root model `y_hat = a + b * sqrt(x)`.

    Parameters
    ----------

    x : np.ndarray
        Independent variable.

    a: float
        Intercept value of the square root model `a + b * sqrt(x)`.

    b: float
        Scaling parameter of independent variable in the square root model `a + b * sqrt(x)`.

    Returns
    -------

    y_hat: np.ndarray
        Predicted `y` values.
    """
    y_hat = a + b * np.sqrt(x)
    return y_hat
