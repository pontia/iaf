import numpy as np


def calc_sse(y: np.ndarray, y_hat: np.ndarray):
    """Calculate the Sum of Squared Errors between prediction `y_hat` and data `y`.

    Parameters
    ----------

    y: np.ndarray
        Array of target values.

    y_hat: np.ndarray
        Array of predicted values.

    Returns
    -------

    sse: float
        Sum of squared errors.
    """

    sse = np.sum(np.power(y_hat - y, 2))
    return sse


def calc_sse_weighed(y: np.ndarray, y_hat: np.ndarray, se: np.ndarray):
    """Calculate the Sum of Squared Errors between prediction `y_hat` and data `y`
    weighed by the standard error vector `se`.

    Parameters
    ----------

    y: np.ndarray
        Array of target values.

    y_hat: np.ndarray
        Array of predicted values.

    se: np.ndarray
        Array of standard error values.

    Returns
    -------

    sse: float
        Weighted sum of squared errors.
    """
    sse = np.sum(np.power(y_hat - y, 2) / np.power(se, 2))
    return sse


def r_squared(y: np.ndarray, y_hat: np.ndarray):
    """Calculate the coefficient of determination `R^2` for vectors `y_hat` and `y`.

    Parameters
    ----------

    y: np.ndarray
        Array of target values.

    y_hat: np.ndarray
        Array of predicted values.

    Returns
    -------

    r_squared: float
        Coefficient of determination `R^2`
    """
    sse = calc_sse(y, y_hat)
    tss = (len(y) - 1) * y.var()
    return 1 - (sse / tss)


def r_squared_adj(y: np.ndarray, y_hat: np.ndarray, p: int):
    """Calculate the adjusted coefficient of determination `R^2` for vectors `y_hat` and `y` and number
     of parameters `p`.

    Parameters
    ----------

    y: np.ndarray
        Array of target values.

    y_hat: np.ndarray
        Array of predicted values.

    p: int
        Number of parameters.

    Returns
    -------

    r_squared: float
        Adjusted coefficient of determination `R^2`
    """
    n = len(y)
    f = (n - 1) / (n - p - 1)
    return f * r_squared(y, y_hat)
