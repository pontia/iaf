from ._metrics import calc_sse, calc_sse_weighed, r_squared, r_squared_adj

__doc__ = "Implementation of simple metrics."
__all__ = ["calc_sse", "calc_sse_weighed", "r_squared", "r_squared_adj"]
