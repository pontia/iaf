import itertools
from typing import Union

import numpy as np
from pystackreg import StackReg

from iaf.color import Color, to_composite
from iaf.io.cast import safe_to_uint8, safe_to_uint16


def find_rigid_transform(A: np.ndarray, B: np.ndarray, force_reflection: bool = False):
    """Calculate rigid transformation to map coordinates A onto coordinates B.

    Parameters
    ----------

    A: np.ndarray
        Array of 2D or 3D source coordinates with shape [2xN] or [3xN]

    B: np.ndarray
        Array of 2D or 3D target coordinates A with shape [2xN] or [3xN]

    force_reflection: bool
        Whether to force reflection if one is suspected: it can give false positives (optional, default False).


    Returns
    -------

    R: np.ndarray
        Rotation matrix (either 2x2 or 3x3) to transform the A coordinates into B.

    t: np.ndarray
        Translation vector (either 2x1 or 3x1) to transform the A coordinates into B.
    """
    assert A.shape == B.shape

    num_rows, num_cols = A.shape
    if num_rows not in [2, 3]:
        raise ValueError(f"matrix A is not 2xN or 3xN, it is {num_rows}x{num_cols}")

    num_rows, num_cols = B.shape
    if num_rows not in [2, 3]:
        raise ValueError(f"matrix B is not 2xN or 3xN, it is {num_rows}x{num_cols}")

    # Number of dimensions
    nDim = num_rows

    # Find column mean
    centroid_A = np.mean(A, axis=1)
    centroid_B = np.mean(B, axis=1)

    # Ensure centroids are column vectors
    centroid_A = centroid_A.reshape(-1, 1)
    centroid_B = centroid_B.reshape(-1, 1)

    # Subtract mean
    Am = A - centroid_A
    Bm = B - centroid_B

    H = Am @ np.transpose(Bm)

    # Sanity check
    rank_H = np.linalg.matrix_rank(H)
    if rank_H < nDim:
        raise ValueError(f"Rank of H = {rank_H}; expected {nDim}.")

    # Find rotation matrix R
    U, S, Vt = np.linalg.svd(H)
    R = Vt.T @ U.T

    # Special reflection case
    if np.linalg.det(R) < 0:
        if force_reflection:
            print("det(R) < R, reflection detected! Correcting...")
            Vt[-1, :] *= -1
            R = Vt.T @ U.T
        else:
            print("det(R) < R, reflection detected! As requested, not correcting...")

    # Calculate the translation vector
    t = -R @ centroid_A + centroid_B

    # Return transposed versions to work on [NxnDim] arrays
    return R, t


def apply_rigid_transform(A: np.ndarray, R: np.ndarray, t: np.ndarray):
    """Applies rotation and translation to coordinates A.

    Parameters
    ----------

    A: np.ndarray
        Array of 2D or 3D source coordinates with shape [2xN] or [3xN]

    R: np.ndarray
        Rotation matrix (2x2 or 3x3).

    t: np.ndarray
        Translation vector (2x1 or 3x1)

    Returns
    -------

    A_t: np.ndarray
        Array of 2D or 3D coordinates (shape [2xN] or [3xN]) transformed by rotation matrix R and translation vector t.
    """

    # Check dimensions
    num_rows, num_cols = A.shape
    if num_rows not in [2, 3]:
        raise ValueError(f"Matrix A is not 2xN or 3xN, it is {num_rows}x{num_cols}")

    if R.shape[0] != R.shape[1] and R.shape[0] != num_rows:
        raise ValueError(f"Rotation matrix R must be {num_cols}x{num_cols}")

    if t.shape[0] != num_rows:
        raise ValueError(f"Translation vector t must be {num_rows}x1")

    # Rotate and translate
    return (R @ A) + t


def multi_image_alignment(
    images: list[np.ndarray], return_composites: bool = False
) -> Union[tuple[list[np.ndarray], np.ndarray, np.ndarray], list[np.ndarray]]:
    """Registers (aligns) a series of images (translation only).

    Parameters
    ----------

    images: list[np.ndarray]
        List of 2D images to register. The first one will be used as template
        against which all others will be registered.

    return_composites: bool = False
        If True, returns two composites of the channels before and after alignment
        for quality control.

    Returns
    -------

    images: list[np.ndarray]
        List of registered images.

    rgb_before: np.ndarray
        Composite of the channels before alignment.

    rgb_after: np.ndarray
        Composite of the channels after alignment.
    """

    if len(images) < 2:
        raise ValueError("At least two images are needed for registration.")

    # Try merging all images into a stack
    try:
        stack = np.zeros(
            shape=(len(images), images[0].shape[0], images[0].shape[1]),
            dtype=images[0].dtype,
        )

        for i, img in enumerate(images):
            stack[i] = img

    except Exception as e:
        raise e

    # Store the datatype before alignment
    dtype = np.dtype(images[0].dtype).type

    # Align to first image
    sr = StackReg(StackReg.TRANSLATION)
    stack_reg = sr.register_transform_stack(stack, reference="first")

    # Extract the images
    images_reg = []
    for i in range(len(images)):
        if dtype is np.uint8:
            converted = safe_to_uint8(stack_reg[i])
        elif dtype is np.uint16:
            converted = safe_to_uint16(stack_reg[i])
        else:
            converted = stack_reg[i]
        images_reg.append(converted)

    # Create composites?
    if return_composites:

        # Prepare colors
        colors = [
            Color.Red,
            Color.Green,
            Color.Blue,
            Color.Gray,
            Color.Orange,
            Color.Yellow,
            Color.Cyan,
        ]

        # Make sure to have enough colors
        cycled_colors = itertools.cycle(colors)
        extended_colors = [next(cycled_colors) for _ in range(len(images))]

        # Create "before" composite
        rgb_before = to_composite(images=images, colors=extended_colors)

        # Create "after" composite
        rgb_after = to_composite(images=images_reg, colors=extended_colors)

        # Return the aligned images and the composites
        return images_reg, rgb_before, rgb_after

    else:

        # Only return the aligned images
        return images_reg
