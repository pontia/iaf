__version__ = "0.5.0"
__doc__ = f"""
This is the documentation of the `iaf` library (version {__version__}).

It is a helper library that adds user-friendly functionality to [scikit-image](https://scikit-image.org/),
[scipy](https://scipy.org/), [matplotlib](https://matplotlib.org/) and other libraries for the purpose of teaching the
fundamentals of image analysis.

You can install it with: 

```bash
$ conda create -n iaf-env python=3.11    # Optional
$ conda activate iaf-env                 # Optional
$ pip install --extra-index-url https://ia-res.ethz.ch/pypi iaf
``` 

Alternatively, you can download and use the following [environment.yml](https://git.bsse.ethz.ch/pontia/iaf/-/raw/master/environment.yml?inline=false) file:

```bash
$ conda env create -f environment.yml
``` 

Currently, `iaf` is compatible with python 3.10, 3.11, and 3.12.
"""
