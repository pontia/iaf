from ._notebook import init_style, set_rc_params

__doc__ = "Jupyter notebook functionalities."
__all__ = ["init_style", "set_rc_params"]
