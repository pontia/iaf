import matplotlib as mpl


def init_style():
    """Set stylesheet and rc parameters for the notebook."""
    set_rc_params()


def set_rc_params():
    """Set figure rendering parameters."""
    mpl.rcParams["figure.figsize"] = (10.0, 6.0)
    mpl.rcParams["figure.autolayout"] = True
    mpl.rcParams["figure.dpi"] = 100
    mpl.rcParams["savefig.dpi"] = 300
