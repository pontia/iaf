from ._color import Color, get_hilo_cmap, get_labels_cmap, to_composite, to_rgb

__doc__ = "Color functions."
__all__ = ["Color", "get_hilo_cmap", "get_labels_cmap", "to_composite", "to_rgb"]
