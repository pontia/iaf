from enum import Enum
from typing import Union

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap


class Color(Enum):
    """A few predefined colors."""

    Red = (1.0, 0.0, 0.0)
    """Red = (1.0, 0.0, 0.0)"""
    Green = (0.0, 1.0, 0.0)
    """Green = (0.0, 1.0, 0.0)"""
    Blue = (0.0, 0.0, 1.0)
    """Blue = (0.0, 0.0, 1.0)"""
    Yellow = (1.0, 1.0, 0.0)
    """Yellow = (1.0, 1.0, 0.0)"""
    Cyan = (0.0, 1.0, 1.0)
    """Cyan = (0.0, 1.0, 1.0)"""
    Orange = (1.0, 0.5, 0.0)
    """Orange = (1.0, 0.5, 0.0)"""
    Gray = (1.0, 1.0, 1.0)
    """Gray = (1.0, 1.0, 1.0)"""


def to_rgb(
    img: np.ndarray, color: Union[Color, list, tuple], clip_percentile: float = 0.1
) -> np.ndarray:
    """
    Create an RGB image from a single-channel image using a specific color.

    Adapted from: https://bioimagebook.github.io/chapters/1-concepts/4-colors/python.html

    Parameters
    ----------

    img: np.ndarray
        2D image to be displayed.

    color: Color, list or tuple
        Values for `(R, G, B)` between 0.0 and 1.0

    clip_percentile: float
        Percentile to clip intensity in the low and high parts of the dynamic range.

    Returns
    -------

    rgb: np.ndarray
        RGB image
    """

    # Check that we do just have a 2D image
    if img.ndim > 2 and img.shape[2] != 1:
        raise ValueError("This function expects a single-channel image!")

    # If color is of class Color, extract the tuple
    if isinstance(color, Color):
        color = color.value

    # Rescale the image according to how we want to display it
    im_scaled = img.astype(np.float32) - np.percentile(img, clip_percentile)
    im_scaled = im_scaled / np.percentile(im_scaled, 100 - clip_percentile)
    im_scaled = np.clip(im_scaled, 0, 1)

    # Need to make sure we have a channels dimension for the multiplication to work
    im_scaled = np.atleast_3d(im_scaled)

    # Reshape the color (here, we assume channels last)
    color = np.asarray(color).reshape((1, 1, -1))
    return im_scaled * color


def to_composite(
    images: list[np.ndarray],
    colors: list[Union[Color, list, tuple]],
    clip_percentile: float = 0.1,
) -> np.ndarray:
    """Create a composite image from a series of gray values images and the corresponding colors.

    Parameters
    ----------

    images: list[np.ndarray]
        List of gray-value images.

    colors: list[Union[Color, list, tuple]]
        List of colors to be used to create a composite. One for each image. Each color is a `Color`, `list` or
        `tuple` with values for `(R, G, B)` between 0.0 and 1.0

    clip_percentile: float
        Percentile to clip intensity in the low and high parts of the dynamic range.

    Returns
    -------

    composite: np.ndarray
        RGB composite image
    """

    # Check the inputs
    if len(images) == 0 or len(images) != len(colors):
        raise ValueError(
            "Please pass a list of images with the corresponding list of colors."
        )

    # Prepare the output
    out = np.zeros((images[0].shape[0], images[0].shape[1], 3), np.float32)

    # Add the images after converting them to RGB
    for image, color in zip(images, colors):
        out += to_rgb(image, color=color, clip_percentile=clip_percentile)

    # Now clip
    return np.clip(out, 0, 1)


def get_labels_cmap(n: int = 256) -> ListedColormap:
    """Return a color map that is particularly suited to color-code labels.

    Parameters
    ----------

    n: int
        Number of different colors (optional, default 256). Use 256 for 8-bit images and 65535 for 16-bit images.

    Returns
    -------

    colormap: ListedColormap
        Colormap to be passed to imshow() and other matplotlib functions that take colormaps.
    """

    # Return a randomly shuffled jet colormap with 256 values,
    # making sure that color 0 is black.
    rng = np.random.default_rng(1975)
    vals = np.linspace(0, 1, n)
    rng.shuffle(vals)
    jet_s = plt.cm.jet(vals)
    black = np.array([0.0, 0.0, 0.0, 1.0]).reshape(1, 4)
    jet = np.concatenate((black, jet_s), axis=0)
    return ListedColormap(jet)


def get_hilo_cmap(n: int = 256) -> ListedColormap:
    """Return the HiLo colormap that shows pixels at 0 intensity as blue and saturated pixels as red.

    Parameters
    ----------

    n: int
        Number of different colors (optional, default 256). Use 256 for 8-bit images and 65535 for 16-bit images.

    Returns
    -------

    colormap: ListedColormap
        Colormap to be passed to `imshow()` and other matplotlib functions that take colormaps.
    """

    vals = np.linspace(0, 1, n - 2)
    gray_s = plt.cm.gray(vals)
    blue = np.array([0.0, 0.0, 1.0, 1.0]).reshape(1, 4)
    red = np.array([1.0, 0.0, 0.0, 1.0]).reshape(1, 4)
    hilo = np.concatenate((blue, gray_s, red), axis=0)
    return ListedColormap(hilo)
