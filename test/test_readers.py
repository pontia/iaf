import unittest
import zipfile
from pathlib import Path

import numpy as np

from iaf.io.readers import HuygensHDF5Reader, ImarisReader, NikonND2Reader
from iaf.io.utils import download_file


class TestIAFReaders(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_nd2_data_folder = None
        self.test_ims_data_folder = None
        self.test_h5_data_folder = None
        self._get_nd2_test_data_if_needed()
        self._get_ims_test_data_if_needed()
        self._get_h5_test_data_if_needed()

    def _get_nd2_test_data_if_needed(self):
        home_folder = Path.home()
        iaf_folder = home_folder / ".iaf"
        test_data_folder = iaf_folder / "nikonnd2reader"
        target_file_name = iaf_folder / "nikonnd2reader.zip"
        if not test_data_folder.exists():
            if not target_file_name.exists():
                print("Downloading test data...")
                target_file_name = download_file(
                    "https://polybox.ethz.ch/index.php/s/nInMKIJK5DhYZ12/download",
                    iaf_folder,
                    "nikonnd2reader.zip",
                )
            with zipfile.ZipFile(target_file_name, "r") as zip_file:
                print("Extracting test data....")
                zip_file.extractall(iaf_folder)

        test_files = [
            "multi_channel.nd2",
            "multi_channel_2.nd2",
            "multi_planes_multi_channels.nd2",
            "multi_planes_multi_channels_2.nd2",
            "multi_series.nd2",
            "multi_series_multi_channels_multi_planes.nd2",
            "multi_series_multi_timepoints.nd2",
        ]
        for test_file in test_files:
            if not (Path(test_data_folder) / test_file).exists():
                raise IOError(f"Test file {test_file} does not exist!")

        # Clean up
        if target_file_name.exists():
            target_file_name.unlink()

        # Set the path
        self.test_nd2_data_folder = test_data_folder

    def _get_ims_test_data_if_needed(self):
        home_folder = Path.home()
        iaf_folder = home_folder / ".iaf"
        test_data_folder = iaf_folder / "imarisreader"
        target_file_name = iaf_folder / "imarisreader.zip"
        if not test_data_folder.exists():
            if not target_file_name.exists():
                print("Downloading test data...")
                target_file_name = download_file(
                    "https://polybox.ethz.ch/index.php/s/mqzPk4cmpef7ztp/download",
                    iaf_folder,
                    "imarisreader.zip",
                )
            with zipfile.ZipFile(target_file_name, "r") as zip_file:
                print("Extracting test data....")
                zip_file.extractall(iaf_folder)

        test_files = [
            "PtK2Cell.ims",
            "SwimmingAlgae.ims",
        ]
        for test_file in test_files:
            if not (Path(test_data_folder) / test_file).exists():
                raise IOError(f"Test file {test_file} does not exist!")

        # Clean up
        if target_file_name.exists():
            target_file_name.unlink()

        # Set the path
        self.test_ims_data_folder = test_data_folder

    def _get_h5_test_data_if_needed(self):
        home_folder = Path.home()
        iaf_folder = home_folder / ".iaf"
        test_data_folder = iaf_folder / "huygenshdf5reader"
        target_file_name = iaf_folder / "huygenshdf5reader.zip"
        if not test_data_folder.exists():
            if not target_file_name.exists():
                print("Downloading test data...")
                target_file_name = download_file(
                    "https://polybox.ethz.ch/index.php/s/6OOV6dfxGoY2ukV/download",
                    iaf_folder,
                    "huygenshdf5reader.zip",
                )
            with zipfile.ZipFile(target_file_name, "r") as zip_file:
                print("Extracting test data....")
                zip_file.extractall(iaf_folder)

        test_files = [
            "Demo_zorigTwoChan_withErrors.h5",
            "U2OS_dataset_02_R3D_original.h5",
            "widefieldTimeSeries.h5",
        ]
        for test_file in test_files:
            if not (Path(test_data_folder) / test_file).exists():
                raise IOError(f"Test file {test_file} does not exist!")

        # Clean up
        if target_file_name.exists():
            target_file_name.unlink()

        # Set the path
        self.test_h5_data_folder = test_data_folder

    def testConstructor(self):
        with self.assertRaises(ValueError):
            _ = NikonND2Reader()
        with self.assertRaises(IOError):
            _ = NikonND2Reader("non_existent_file.nd2")
        with self.assertRaises(ValueError):
            _ = NikonND2Reader(Path(__file__).parent / "data" / "cells_shaded.tif")
        with self.assertRaises(ValueError):
            _ = ImarisReader()
        with self.assertRaises(IOError):
            _ = ImarisReader("non_existent_file.ims")
        with self.assertRaises(ValueError):
            _ = ImarisReader(Path(__file__).parent / "data" / "cells_shaded.tif")

    def testND2Iterator(self):
        reader = NikonND2Reader(
            self.test_nd2_data_folder / "multi_series_multi_timepoints.nd2"
        )
        self.assertEqual(reader.geometry, "tczyx")
        for i, img in enumerate(reader):
            self.assertEqual(img.shape, (7, 3, 6, 512, 512))

    def testND2(self):
        reader = NikonND2Reader(self.test_nd2_data_folder / "multi_series.nd2")
        self.assertEqual(reader.num_series, 40)
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 3)
        self.assertEqual(reader.num_planes, 1)
        self.assertEqual(reader.geometry, "cyx")
        self.assertEqual(reader.iter_axis, "v")
        self.assertEqual(reader.metadata["pixel_microns"], 0.647167216011023)
        self.assertTrue(
            np.allclose(
                reader.voxel_sizes, (0.647167216011023, 0.647167216011023, 0.0), 1e-10
            )
        )
        self.assertEqual(reader.channel_names, ("SCF_DAPI", "SCF_GFP", "SCF_Cy5"))
        indices = [0, 39]
        for index in indices:
            img = reader[index]
            self.assertEqual(len(img.shape), 3)
            self.assertEqual(img.shape[0], 3)
            self.assertEqual(img.shape[1], 3884)
            self.assertEqual(img.shape[2], 3892)
        del reader

        reader = NikonND2Reader(
            self.test_nd2_data_folder / "multi_series_multi_timepoints.nd2"
        )
        self.assertEqual(reader.num_series, 5)
        self.assertEqual(reader.num_timepoints, 7)
        self.assertEqual(reader.num_channels, 3)
        self.assertEqual(reader.num_planes, 6)
        self.assertEqual(reader.geometry, "tczyx")
        self.assertEqual(reader.iter_axis, "v")
        self.assertEqual(reader.metadata["pixel_microns"], 0.12119355877578743)
        self.assertTrue(
            np.allclose(
                reader.voxel_sizes,
                (0.12119355877578743, 0.12119355877578743, 1.4995714285714297),
                1e-10,
            )
        )
        self.assertEqual(reader.channel_names, ("DAPI", "DiR", "TD"))
        indices = [0, 4]
        for index in indices:
            img = reader[index]
            self.assertEqual(len(img.shape), 5)
            self.assertEqual(img.shape[0], 7)  # T
            self.assertEqual(img.shape[1], 3)  # C
            self.assertEqual(img.shape[2], 6)  # Z
            self.assertEqual(img.shape[3], 512)  # Y
            self.assertEqual(img.shape[4], 512)  # X
        del reader

        reader = NikonND2Reader(self.test_nd2_data_folder / "multi_channel.nd2")
        self.assertEqual(reader.num_series, 1)
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 3)
        self.assertEqual(reader.num_planes, 1)
        self.assertEqual(reader.geometry, "cyx")
        self.assertEqual(reader.iter_axis, "t")
        self.assertEqual(reader.metadata["pixel_microns"], 0.1241751041301025)
        self.assertTrue(
            np.allclose(
                reader.voxel_sizes, (0.1241751041301025, 0.1241751041301025, 0.0), 1e-10
            )
        )
        self.assertEqual(reader.channel_names, ("DAPI", "EGFP", "mCherry"))
        indices = [0]
        for index in indices:
            img = reader[index]
            self.assertEqual(len(img.shape), 3)
            self.assertEqual(img.shape[0], 3)  # C
            self.assertEqual(img.shape[1], 1024)  # Y
            self.assertEqual(img.shape[2], 1024)  # X
        del reader

        reader = NikonND2Reader(self.test_nd2_data_folder / "multi_channel_2.nd2")
        self.assertEqual(reader.num_series, 1)
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 5)
        self.assertEqual(reader.num_planes, 1)
        self.assertEqual(reader.geometry, "cyx")
        self.assertEqual(reader.iter_axis, "t")
        self.assertEqual(reader.metadata["pixel_microns"], 0.8625684516339195)
        self.assertTrue(
            np.allclose(
                reader.voxel_sizes, (0.8625684516339195, 0.8625684516339195, 0.0), 1e-10
            )
        )
        self.assertEqual(
            reader.channel_names,
            (
                "DAPI",
                "Fluorescein goat anti–mouse IgG antibody/pH 8.0",
                "Cy3 dye–labeled IgG antibody/pH 7.2",
                "DiR",
                "TD",
            ),
        )
        indices = [0]
        for index in indices:
            img = reader[index]
            self.assertEqual(len(img.shape), 3)
            self.assertEqual(img.shape[0], 5)  # C
            self.assertEqual(img.shape[1], 512)  # Y
            self.assertEqual(img.shape[2], 512)  # X
        del reader

        reader = NikonND2Reader(
            self.test_nd2_data_folder / "multi_planes_multi_channels.nd2"
        )
        self.assertEqual(reader.num_series, 1)
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 5)
        self.assertEqual(reader.num_planes, 45)
        self.assertEqual(reader.geometry, "czyx")
        self.assertEqual(reader.iter_axis, "t")
        self.assertEqual(reader.metadata["pixel_microns"], 1.2313500423051482)
        self.assertTrue(
            np.allclose(
                reader.voxel_sizes, (1.2313500423051482, 1.2313500423051482, 0.0), 1e-10
            )
        )
        self.assertEqual(
            reader.channel_names, ("EGFP", "mCherry", "TD", "EGFP", "mCherry")
        )
        indices = [0]
        for index in indices:
            img = reader[index]
            self.assertEqual(len(img.shape), 4)
            self.assertEqual(img.shape[0], 5)  # C
            self.assertEqual(img.shape[1], 45)  # z
            self.assertEqual(img.shape[2], 512)  # Y
            self.assertEqual(img.shape[3], 512)  # X
        del reader

        reader = NikonND2Reader(
            self.test_nd2_data_folder / "multi_planes_multi_channels_2.nd2"
        )
        self.assertEqual(reader.num_series, 1)
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 4)
        self.assertEqual(reader.num_planes, 37)
        self.assertEqual(reader.geometry, "czyx")
        self.assertEqual(reader.iter_axis, "t")
        self.assertEqual(reader.metadata["pixel_microns"], 1.2313500423051482)
        self.assertTrue(
            np.allclose(
                reader.voxel_sizes, (1.2313500423051482, 1.2313500423051482, 0.0), 1e-10
            )
        )
        self.assertEqual(reader.channel_names, ("EGFP", "mCherry", "EGFP", "mCherry"))
        indices = [0]
        for index in indices:
            img = reader[index]
            self.assertEqual(len(img.shape), 4)
            self.assertEqual(img.shape[0], 4)  # C
            self.assertEqual(img.shape[1], 37)  # z
            self.assertEqual(img.shape[2], 512)  # Y
            self.assertEqual(img.shape[3], 512)  # X
        del reader

        reader = NikonND2Reader(
            self.test_nd2_data_folder / "multi_series_multi_channels_multi_planes.nd2"
        )
        self.assertEqual(reader.num_series, 7)
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 2)
        self.assertEqual(reader.num_planes, 6)
        self.assertEqual(reader.geometry, "czyx")
        self.assertEqual(reader.iter_axis, "v")
        self.assertEqual(reader.metadata["pixel_microns"], 0.5196325831122398)
        self.assertTrue(
            np.allclose(
                reader.voxel_sizes,
                (0.5196325831122398, 0.5196325831122398, 4.9549999999999725),
                1e-10,
            )
        )
        self.assertEqual(reader.channel_names, ("EGFP", "mCherry"))
        indices = [0, 6]
        for index in indices:
            img = reader[index]
            self.assertEqual(len(img.shape), 4)
            self.assertEqual(img.shape[0], 2)  # C
            self.assertEqual(img.shape[1], 6)  # z
            self.assertEqual(img.shape[2], 1024)  # Y
            self.assertEqual(img.shape[3], 1024)  # X
        del reader

    def testIMS(self):
        reader = ImarisReader(self.test_ims_data_folder / "PtK2Cell.ims")
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 4)
        self.assertEqual(reader.num_planes, 32)
        self.assertEqual(reader.size, (256, 256, 32))
        self.assertTrue(np.allclose(reader.voxel_sizes, (0.0670, 0.0670, 0.2), 1e-4))
        self.assertTrue(
            np.allclose(
                reader.extends, (-0.0330, 17.118, -0.0330, 17.118, -0.100, 6.30), 1e-4
            )
        )
        self.assertEqual(
            reader.channel_names, ("Kinetochores", "Microtubules", "DNA", "Dynactin")
        )
        data = reader.load(timepoint=0, channel=0)
        self.assertTrue(data.dtype == np.uint8)
        self.assertEqual(data.shape, (32, 256, 256))
        del reader

        reader = ImarisReader(self.test_ims_data_folder / "SwimmingAlgae.ims")
        self.assertEqual(reader.num_timepoints, 45)
        self.assertEqual(reader.num_channels, 1)
        self.assertEqual(reader.num_planes, 1)
        self.assertEqual(reader.size, (448, 262, 1))
        self.assertTrue(np.allclose(reader.voxel_sizes, (1.00, 1.00, 1.00), 1e-4))
        self.assertTrue(
            np.allclose(reader.extends, (-0.500, 447.5, 59.5, 321.5, 0.0, 1.0), 1e-4)
        )
        self.assertEqual(reader.channel_names, ("(name not specified)",))
        data = reader.load(timepoint=0, channel=0)
        self.assertTrue(data.dtype == np.uint8)
        self.assertEqual(data.shape, (262, 448))
        del reader

    def testH5(self):
        reader = HuygensHDF5Reader(
            self.test_h5_data_folder / "U2OS_dataset_02_R3D_original.h5"
        )
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 4)
        self.assertEqual(reader.num_planes, 30)
        self.assertEqual(reader.size, (384, 384, 30))
        self.assertTrue(
            np.allclose(reader.voxel_sizes, (6.4582e-8, 6.4582e-8, 2e-7), 1e-6)
        )
        self.assertEqual(
            reader.channel_names,
            ("DAPI", "GFP", "Alexa Fluor 568", "NovaFluor Red 685"),
        )
        data = reader.load(timepoint=0, channel=0)
        self.assertTrue(data.dtype == np.int16)  # Signed integer 16!
        self.assertEqual(data.shape, (30, 384, 384))
        self.assertTrue(len(reader.metadata.keys()) == 44)
        self.assertTrue(
            np.allclose(reader.metadata["NumericalAperture"], (1.4, 1.4, 1.4, 1.4))
        )
        self.assertEqual(
            reader.metadata["MicroscopeTypeStr"],
            "widefield widefield widefield widefield",
        )
        self.assertTrue(
            np.allclose(
                reader.metadata["ExcitationWavelength"],
                (4.2e-07, 4.86e-07, 5.68e-07, 6.3e-07),
                1e-6,
            )
        )
        del reader

        reader = HuygensHDF5Reader(
            self.test_h5_data_folder / "Demo_zorigTwoChan_withErrors.h5"
        )
        self.assertEqual(reader.num_timepoints, 1)
        self.assertEqual(reader.num_channels, 2)
        self.assertEqual(reader.num_planes, 32)
        self.assertEqual(reader.size, (64, 128, 32))
        self.assertTrue(
            np.allclose(reader.voxel_sizes, (4.7602e-8, 2e-7, 1.5e-7), 1e-6)
        )
        self.assertEqual(reader.channel_names, ("", ""))
        data = reader.load(timepoint=0, channel=0)
        self.assertTrue(data.dtype == np.float32)
        self.assertEqual(data.shape, (32, 128, 64))
        self.assertTrue(len(reader.metadata.keys()) == 20)
        self.assertTrue(np.allclose(reader.metadata["NumericalAperture"], (1.45, 1.45)))
        self.assertEqual(
            reader.metadata["MicroscopeTypeStr"],
            "confocal confocal",
        )
        self.assertTrue(
            np.allclose(
                reader.metadata["ExcitationWavelength"],
                (4.88e-07, 4.88e-07),
                1e-6,
            )
        )
        del reader

        reader = HuygensHDF5Reader(self.test_h5_data_folder / "widefieldTimeSeries.h5")
        self.assertEqual(reader.num_timepoints, 4)
        self.assertEqual(reader.num_channels, 1)
        self.assertEqual(reader.num_planes, 19)
        self.assertEqual(reader.size, (300, 440, 19))
        self.assertTrue(
            np.allclose(reader.voxel_sizes, (1.34e-7, 1.34e-7, 2.5e-7), 1e-6)
        )
        self.assertEqual(reader.channel_names, ("",))
        data = reader.load(timepoint=0, channel=0)
        self.assertTrue(data.dtype == np.float32)
        self.assertEqual(data.shape, (19, 440, 300))
        self.assertTrue(len(reader.metadata.items()) == 20)
        self.assertTrue(np.allclose(reader.metadata["NumericalAperture"], (1.3,)))
        self.assertEqual(
            reader.metadata["MicroscopeTypeStr"],
            "widefield",
        )
        self.assertTrue(
            np.allclose(
                reader.metadata["ExcitationWavelength"],
                (4.783999999999999e-07,),
                1e-6,
            )
        )
        del reader


if __name__ == "__main__":
    unittest.main()
