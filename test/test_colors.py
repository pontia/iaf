import unittest
from pathlib import Path

import numpy as np
from skimage.io import imread

from iaf.color import Color, to_composite, to_rgb
from iaf.plot._plot import _stretch_and_convert


class TestColors(unittest.TestCase):
    def testToRGB(self):
        # Read the image to process
        img = imread(Path(__file__).parent / "data" / "Rat_Hippocampal_Neuron.tif")

        for i in range(img.shape[0]):
            rgb1 = to_rgb(img[i], [1.0, 0.0, 0.0])
            rgb2 = to_rgb(img[i], (1.0, 0.0, 0.0))
            rgb3 = to_rgb(img[i], Color.Red)
            rgb4 = to_rgb(img[i], (1.0, 0.0, 0.0), clip_percentile=0.1)

            self.assertTrue(np.all(rgb1 == rgb2))
            self.assertTrue(np.all(rgb1 == rgb3))
            self.assertTrue(np.all(rgb1 == rgb4))

    def testToComposite(self):
        # Read the image to process
        img = imread(Path(__file__).parent / "data" / "Rat_Hippocampal_Neuron.tif")

        composite1 = to_composite(
            images=[img[0], img[1], img[2], img[3], img[4]],
            colors=[Color.Red, Color.Green, Color.Blue, Color.Yellow, Color.Gray],
        )
        composite2 = to_composite(
            images=[img[0], img[1], img[2], img[3], img[4]],
            colors=[
                (1.0, 0.0, 0.0),
                (0.0, 1.0, 0.0),
                (0.0, 0.0, 1.0),
                (1.0, 1.0, 0.0),
                (1.0, 1.0, 1.0),
            ],
        )
        composite3 = to_composite(
            images=[img[0], img[1], img[2], img[3], img[4]],
            colors=[
                [1.0, 0.0, 0.0],
                [0.0, 1.0, 0.0],
                [0.0, 0.0, 1.0],
                [1.0, 1.0, 0.0],
                [1.0, 1.0, 1.0],
            ],
            clip_percentile=0.1,
        )
        composite4 = to_composite(
            [img[0], img[1], img[2], img[3], img[4]],
            [
                [1.0, 0.0, 0.0],
                [0.0, 1.0, 0.0],
                [0.0, 0.0, 1.0],
                [1.0, 1.0, 0.0],
                [1.0, 1.0, 1.0],
            ],
            0.1,
        )

        self.assertTrue(np.all(composite1 == composite2))
        self.assertTrue(np.all(composite1 == composite3))
        self.assertTrue(np.all(composite1 == composite4))

    def testStretchAndConversionNoClipping(self):
        # Synthetic images to process

        # Gray 8bit
        img_8bit = np.ones((100, 100), dtype=np.uint8)
        img_8bit[:, :50] = 50
        img_8bit[:, 50:] = 200

        out, vmin, vmax = _stretch_and_convert(img_8bit, auto_stretch=False)
        self.assertTrue(np.all(out[:, :50] == 50))
        self.assertTrue(np.all(out[:, 50:] == 200))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        out, vmin, vmax = _stretch_and_convert(img_8bit, auto_stretch=True)
        self.assertTrue(np.all(out[:, :50] == 0))
        self.assertTrue(np.all(out[:, 50:] == 255))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        # Gray 16bit
        img_16bit = np.ones((100, 100), dtype=np.uint16)
        img_16bit[:, :50] = 5000
        img_16bit[:, 50:] = 10000

        out, vmin, vmax = _stretch_and_convert(img_16bit, auto_stretch=False)
        self.assertTrue(
            np.all(out[:, :50] == np.uint8(255.0 * (5000.0 - 0.0) / (65535 - 0.0)))
        )
        self.assertTrue(
            np.all(out[:, 50:] == np.uint8(255.0 * (10000.0 - 0.0) / (65535 - 0.0)))
        )
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        out, vmin, vmax = _stretch_and_convert(img_16bit, auto_stretch=True)
        self.assertTrue(np.all(out[:, :50] == 0))
        self.assertTrue(np.all(out[:, 50:] == 255))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        # RGB 8bit
        img_RGB_8bit = np.ones((100, 100, 3), dtype=np.uint8)
        img_RGB_8bit[:, :, 0] = 10
        img_RGB_8bit[:, :, 1] = 50
        img_RGB_8bit[:, :, 2] = 100

        out, vmin, vmax = _stretch_and_convert(img_RGB_8bit, auto_stretch=False)
        self.assertTrue(np.all(out[:, :, 0] == 10))
        self.assertTrue(np.all(out[:, :, 1] == 50))
        self.assertTrue(np.all(out[:, :, 2] == 100))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        out, vmin, vmax = _stretch_and_convert(img_RGB_8bit, auto_stretch=True)
        self.assertTrue(np.all(out[:, :, 0] == 0))
        self.assertTrue(
            np.all(out[:, :, 1] == np.uint8(255.0 * (50.0 - 10.0) / (100.0 - 10.0)))
        )
        self.assertTrue(np.all(out[:, :, 2] == 255))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        # RGB 16bit
        img_RGB_16bit = np.ones((100, 100, 3), dtype=np.uint16)
        img_RGB_16bit[:, :, 0] = 1000
        img_RGB_16bit[:, :, 1] = 5000
        img_RGB_16bit[:, :, 2] = 10000

        out, vmin, vmax = _stretch_and_convert(img_RGB_16bit, auto_stretch=False)
        self.assertTrue(
            np.all(out[:, :, 0] == np.uint8(255.0 * (1000.0 - 0.0) / (65535.0 - 0.0)))
        )
        self.assertTrue(
            np.all(out[:, :, 1] == np.uint8(255.0 * (5000.0 - 0.0) / (65535.0 - 0.0)))
        )
        self.assertTrue(
            np.all(out[:, :, 2] == np.uint8(255.0 * (10000.0 - 0.0) / (65535.0 - 0.0)))
        )
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        out, vmin, vmax = _stretch_and_convert(img_RGB_16bit, auto_stretch=True)
        self.assertTrue(np.all(out[:, :, 0] == 0))
        self.assertTrue(
            np.all(
                out[:, :, 1] == np.uint8(255.0 * (5000.0 - 1000.0) / (10000.0 - 1000.0))
            )
        )
        self.assertTrue(np.all(out[:, :, 2] == 255))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        # Float in 0..1 range
        img_norm_32bit = np.ones((100, 100), dtype=np.float32)
        img_norm_32bit[:, :50] = 0.2
        img_norm_32bit[:, 50:] = 0.7

        out, vmin, vmax = _stretch_and_convert(img_norm_32bit, auto_stretch=False)
        self.assertTrue(np.all(img_norm_32bit == out))
        self.assertTrue(vmin is None)
        self.assertTrue(vmax is None)

        # Float outsude 0..1 range
        img_norm_32bit = np.ones((100, 100), dtype=np.float32)
        img_norm_32bit[:, :50] = 2000
        img_norm_32bit[:, 50:] = 7000

        out, vmin, vmax = _stretch_and_convert(img_norm_32bit, auto_stretch=False)
        self.assertTrue(np.all(img_norm_32bit == out))
        self.assertTrue(vmin is None)
        self.assertTrue(vmax is None)

    def testStretchAndConversionWithClipping(self):
        # 8 bit
        img_8bit = np.arange(start=50, stop=200, step=10, dtype=np.uint8)
        out, vmin, vmax = _stretch_and_convert(img_8bit, auto_stretch=False)
        self.assertTrue(np.all(img_8bit == out))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        img_8bit = np.arange(start=50, stop=200, step=10, dtype=np.uint8)
        out, vmin, vmax = _stretch_and_convert(img_8bit, auto_stretch=True)
        exp_out = np.array(
            [0, 18, 36, 54, 72, 91, 109, 127, 145, 163, 182, 200, 218, 236, 255],
            np.uint8,
        )
        self.assertTrue(np.all(exp_out == out))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        img_8bit = np.arange(start=50, stop=200, step=10, dtype=np.uint8)
        out, vmin, vmax = _stretch_and_convert(
            img_8bit, auto_stretch=True, clip_percentile=0.0
        )
        exp_out = np.array(
            [0, 18, 36, 54, 72, 91, 109, 127, 145, 163, 182, 200, 218, 236, 255],
            np.uint8,
        )
        self.assertTrue(np.all(exp_out == out))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        img_8bit = np.arange(start=50, stop=200, step=10, dtype=np.uint8)
        out, vmin, vmax = _stretch_and_convert(
            img_8bit, auto_stretch=True, clip_percentile=14.28575
        )  # 70 - 170
        exp_out = np.array(
            [0, 0, 0, 25, 50, 76, 101, 127, 153, 178, 204, 229, 255, 255, 255], np.uint8
        )
        self.assertTrue(np.all(exp_out == out))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        # 16 bit
        img_16bit = np.arange(start=5000, stop=20000, step=1000, dtype=np.uint16)
        out, vmin, vmax = _stretch_and_convert(img_16bit, auto_stretch=False)
        exp_out = np.array([19, 23, 27, 31, 35, 38, 42, 46, 50, 54, 58, 62, 66, 70, 73])
        self.assertTrue(np.all(exp_out == out))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        img_16bit = np.arange(start=5000, stop=20000, step=1000, dtype=np.uint16)
        out, vmin, vmax = _stretch_and_convert(img_16bit, auto_stretch=True)
        exp_out = np.array(
            [0, 18, 36, 54, 72, 91, 109, 127, 145, 163, 182, 200, 218, 236, 255],
            np.uint8,
        )
        self.assertTrue(np.all(exp_out == out))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        img_16bit = np.arange(start=5000, stop=20000, step=1000, dtype=np.uint16)
        out, vmin, vmax = _stretch_and_convert(
            img_16bit, auto_stretch=True, clip_percentile=0.0
        )
        exp_out = np.array(
            [0, 18, 36, 54, 72, 91, 109, 127, 145, 163, 182, 200, 218, 236, 255],
            np.uint8,
        )
        self.assertTrue(np.all(exp_out == out))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        img_16bit = np.arange(start=5000, stop=20000, step=1000, dtype=np.uint16)
        out, vmin, vmax = _stretch_and_convert(
            img_16bit, auto_stretch=True, clip_percentile=14.28575
        )  # 7000 - 17000
        exp_out = np.array(
            [0, 0, 0, 25, 50, 76, 101, 127, 153, 178, 204, 229, 255, 255, 255], np.uint8
        )
        self.assertTrue(np.all(exp_out == out))
        self.assertTrue(vmin == 0)
        self.assertTrue(vmax == 255)

        # float

        img_float32 = np.arange(start=50, stop=200, step=10, dtype=np.float32)
        # float32: auto_stretch and clip_percentile are ignored
        out, vmin, vmax = _stretch_and_convert(
            img_float32, auto_stretch=True, clip_percentile=14.28575
        )
        self.assertTrue(np.all(img_float32 == out))
        self.assertTrue(vmin is None)
        self.assertTrue(vmax is None)

        img_float32 = np.arange(start=5000, stop=20000, step=1000, dtype=np.float32)
        # float32: auto_stretch and clip_percentile are ignored
        out, vmin, vmax = _stretch_and_convert(
            img_float32, auto_stretch=True, clip_percentile=14.28575
        )
        self.assertTrue(np.all(img_float32 == out))
        self.assertTrue(vmin is None)
        self.assertTrue(vmax is None)

        img_float64 = np.arange(start=50, stop=200, step=10, dtype=np.float64)
        # float64: auto_stretch and clip_percentile are ignored
        out, vmin, vmax = _stretch_and_convert(
            img_float64, auto_stretch=True, clip_percentile=14.28575
        )
        self.assertTrue(np.all(img_float64 == out))
        self.assertTrue(vmin is None)
        self.assertTrue(vmax is None)

        img_float64 = np.arange(start=5000, stop=20000, step=1000, dtype=np.float64)
        # float64: auto_stretch and clip_percentile are ignored
        out, vmin, vmax = _stretch_and_convert(
            img_float64, auto_stretch=True, clip_percentile=14.28575
        )
        self.assertTrue(np.all(img_float64 == out))
        self.assertTrue(vmin is None)
        self.assertTrue(vmax is None)
