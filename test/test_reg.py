import unittest
from pathlib import Path

import numpy as np
from tifffile import imread

from iaf.reg import apply_rigid_transform, find_rigid_transform, multi_image_alignment


class TestRegistrations(unittest.TestCase):
    def testRigidRegistrationTranslationOnly(self):
        # Source coordinates
        A = np.array([[0, 0], [0, 1], [1, 0]])

        # Target coordinates B (translated by [0.25, 0.25] with respect to A)
        B = A + np.array([[0.25, 0.25]])

        # Find rigid trasnform
        R, t = find_rigid_transform(A.T, B.T)

        # Check
        R_exp = np.array([[1.0, 0.0], [0.0, 1.0]])
        self.assertTrue(np.allclose(R, R_exp, atol=1e-6))

        t_exp = np.array([[0.25], [0.25]])
        self.assertTrue(np.allclose(t, t_exp, atol=1e-6))

        # Apply correction
        A_t = apply_rigid_transform(A.T, R, t)

        # Check transformation result (A_t must be very close to B)
        self.assertTrue(np.allclose(A_t.T, B, atol=1e-6))

    def testRigidRegistrationRotationOnly(self):
        # Source coordinates
        A = np.array([[0, 0], [0, 1], [1, 0]])

        # Counterclockwise 30-degree rotation
        R_exp = np.array(
            [
                [np.cos(np.pi / 6), np.sin(np.pi / 6)],
                [-np.sin(np.pi / 6), np.cos(np.pi / 6)],
            ]
        )

        # Target coordinates B (rotates 30 degrees counterclockwise with respect to A)
        B = (R_exp @ A.T).T

        # Find rigid trasnform
        R, t = find_rigid_transform(A.T, B.T)

        # Expected rotation matrix and translation vector
        self.assertTrue(np.allclose(R, R_exp, atol=1e-6))

        t_exp = np.array([[0.0], [0.0]])
        self.assertTrue(np.allclose(t, t_exp, atol=1e-6))

        # Apply correction
        A_t = apply_rigid_transform(A.T, R, t)

        # Check transformation result (A_t must be very close to B)
        self.assertTrue(np.allclose(A_t.T, B, atol=1e-6))

    def testRigidRegistrationRotationPlusTranslation(self):
        # Source coordinates
        A = np.array([[0, 0], [0, 1], [1, 0]])

        # Counterclockwise 30-degree rotation
        R_exp = np.array(
            [
                [np.cos(np.pi / 6), np.sin(np.pi / 6)],
                [-np.sin(np.pi / 6), np.cos(np.pi / 6)],
            ]
        )

        # Translate by 0.25 on both axes
        t_exp = np.array([[0.0], [0.0]])

        # Target coordinates B (rotates 30 degrees counterclockwise with respect to A)
        B = (R_exp @ A.T + t_exp).T

        # Find rigid transform
        R, t = find_rigid_transform(A.T, B.T)

        # Expected rotation matrix and translation vector
        self.assertTrue(np.allclose(R, R_exp, atol=1e-6))
        self.assertTrue(np.allclose(t, t_exp, atol=1e-6))

        # Apply correction
        A_t = apply_rigid_transform(A.T, R, t)

        # Check transformation result (A_t must be very close to B)
        self.assertTrue(np.allclose(A_t.T, B, atol=1e-6))

    def testMultiImageAlignment(self):

        # Read images
        cy3 = imread(Path(__file__).parent / "data" / "align_cy3.tif")
        gfp = imread(Path(__file__).parent / "data" / "align_gfp.tif")
        dapi = imread(Path(__file__).parent / "data" / "align_dapi.tif")

        # Check
        self.assertTrue(cy3.shape == (2048, 2048))
        self.assertTrue(gfp.shape == (2048, 2048))
        self.assertTrue(dapi.shape == (2048, 2048))

        # Align them
        aligned = multi_image_alignment([cy3, gfp, dapi])

        self.assertTrue(type(aligned) is list)
        self.assertTrue(len(aligned) == 3)
        self.assertTrue(aligned[0].dtype == np.uint16)
        self.assertTrue(aligned[1].dtype == np.uint16)
        self.assertTrue(aligned[2].dtype == np.uint16)
        self.assertTrue(aligned[0].min() == 0)
        self.assertTrue(aligned[0].max() == 6360)
        self.assertTrue(aligned[1].min() == 0)
        self.assertTrue(aligned[1].max() == 7854)
        self.assertTrue(aligned[2].min() == 0)
        self.assertTrue(aligned[2].max() == 8555)

        # Align them and retrieve composites
        aligned_more, rgb_before, rgb_after = multi_image_alignment(
            [cy3, gfp, dapi], return_composites=True
        )

        self.assertTrue(type(aligned_more) is list)
        self.assertTrue(type(rgb_before) is np.ndarray)
        self.assertTrue(type(rgb_after) is np.ndarray)
        self.assertTrue(aligned_more[0].shape == (2048, 2048))
        self.assertTrue(aligned_more[1].shape == (2048, 2048))
        self.assertTrue(aligned_more[2].shape == (2048, 2048))
        self.assertTrue(aligned[0].dtype == np.uint16)
        self.assertTrue(aligned[1].dtype == np.uint16)
        self.assertTrue(aligned[2].dtype == np.uint16)
        self.assertTrue(rgb_before.shape == (2048, 2048, 3))
        self.assertTrue(rgb_after.shape == (2048, 2048, 3))
        self.assertTrue(np.all(aligned[0] == aligned_more[0]))
        self.assertTrue(np.all(aligned[1] == aligned_more[1]))
        self.assertTrue(np.all(aligned[2] == aligned_more[2]))
