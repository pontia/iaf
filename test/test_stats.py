import unittest

import numpy as np

from iaf.stats import prepare_histogram


class TestStats(unittest.TestCase):
    def test_histograms_and_bins(self):
        # Prepare data
        n_values = 100000
        rnd = np.random.RandomState(seed=2023)
        data = rnd.randint(0, 1000, size=n_values, dtype=int)

        self.assertTrue(len(data) == n_values, "Wrong number of data points.")

        # Calculate the normalized histogram

        # Use 1.0 bins
        n, bin_edges, bin_centers, bin_width = prepare_histogram(
            data, auto_bins=False, bin_size=1.0, normalize=False
        )

        self.assertTrue(len(n) == 1000)
        self.assertTrue(bin_edges[0] == -0.5)
        self.assertTrue(bin_edges[-1] == 999.5)
        self.assertTrue(bin_centers[0] == 0.0)
        self.assertTrue(bin_centers[-1] == 999)
        self.assertTrue(
            np.all(n[:10] == [106, 121, 127, 94, 90, 122, 118, 111, 105, 98])
        )
        self.assertTrue(
            np.all(n[-10:] == [105, 124, 110, 104, 97, 99, 100, 119, 105, 92])
        )

        # Use 1.0 bins and normalize
        n, bin_edges, bin_centers, bin_width = prepare_histogram(
            data, auto_bins=False, bin_size=1.0, normalize=True
        )
        self.assertTrue(np.isclose(n.sum(), 1.0))

        # Use automatic bin_size determination
        n, bin_edges, bin_centers, bin_width = prepare_histogram(data, auto_bins=True)

        self.assertTrue(len(n) == 47)
        self.assertTrue(np.isclose(bin_edges[0], -10.79371779705974))
        self.assertTrue(np.isclose(bin_edges[-1], 1003.8157551265559))
        self.assertTrue(np.isclose(bin_centers[0], 0.0))
        self.assertTrue(np.isclose(bin_centers[-1], 993.0220373294962))
        self.assertTrue(
            np.all(
                n[:10]
                == [
                    0.01203,
                    0.02182,
                    0.02098,
                    0.02212,
                    0.02195,
                    0.02116,
                    0.02167,
                    0.02015,
                    0.02282,
                    0.02201,
                ]
            )
        )
        self.assertTrue(
            np.all(
                n[-10:]
                == [
                    0.02191,
                    0.02192,
                    0.02102,
                    0.02242,
                    0.02066,
                    0.02195,
                    0.02221,
                    0.02123,
                    0.02223,
                    0.01752,
                ]
            )
        )

        # Use auto bins and normalize
        n, bin_edges, bin_centers, bin_width = prepare_histogram(
            data, auto_bins=True, normalize=True
        )
        self.assertTrue(np.isclose(n.sum(), 1.0))


if __name__ == "__main__":
    unittest.main()
