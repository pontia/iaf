import platform
import unittest
from pathlib import Path
from tempfile import TemporaryDirectory

import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize
from skimage.io import imread
from skimage.measure import label

from iaf.color import get_labels_cmap
from iaf.fit.metrics import calc_sse
from iaf.fit.models import linear_model
from iaf.plot import add_fit, plot_data, show_labels
from iaf.plot.utils import plot_parameter_search_2d


class TestPlot(unittest.TestCase):
    def test_sse(self):
        a_real = 3.0
        b_real = 10.0
        noise_level = 1.0

        a = 1.5
        b = 0.0
        start = -10.0
        stop = 10.0
        num = 101

        rng = np.random.RandomState(1)

        # Number of points
        x = np.linspace(start=start, stop=stop, num=num)
        y = a_real * x + b_real + noise_level * rng.randn(len(x))

        # "Predict"
        y_hat = linear_model(a, x, b)

        # Calc the sse
        sse = calc_sse(y_hat, y)

        sse_real = 18169.485449511147
        self.assertEqual(sse_real, sse)

        # Plot
        if platform.system() != "Linux":
            assert True
            return

        with TemporaryDirectory() as tmp_dir:
            plot_data(
                x,
                y,
                y_hat=y_hat,
                a=a,
                b=b,
                sse=sse,
                figure_size=(16, 8),
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_sse_fig_01.tif",
            )
            img = imread(Path(tmp_dir) / "test_sse_fig_01.tif")
        exp_img = imread(Path(__file__).parent / "data" / "exp_test_sse_fig_01.tif")
        self.assertTrue(np.all(img == exp_img))

        # Close all figures
        plt.close("all")

    def test_see_weighed(self):
        a_real = 3
        b_real = 10

        rng = np.random.RandomState(1)

        nReps = 5
        w0 = 3
        w1 = 0.5
        w2 = 10
        data_offset = 10

        x = np.linspace(start=0, stop=20, num=21)

        n1 = len(x) // 2
        n2 = len(x) - n1

        yy = np.zeros((nReps, len(x)))
        for i in range(nReps):
            noise = np.zeros(len(x))
            noise[:n1] = w1 * rng.randn(n1)
            noise[n1:] = data_offset + w2 * rng.randn(n2)
            yy[i, :] = a_real * x + b_real + w0 * noise

        yy_mean = yy.mean(axis=0)

        sd = yy.std(axis=0, ddof=1)
        se = sd / np.sqrt(nReps)

        def calc_sse_weighed(y, y_hat, se):
            """Calculate the Sum of Squared Errors between prediction y_hat and data y."""
            sse = np.sum(np.power(y_hat - y, 2) / np.power(se, 2))
            return sse

        def linear_model(a, x, b):
            """Our linear model y = a * x + b."""
            y_hat = a * x + b
            return y_hat

        def objf_weighed(params, x, y, se):
            """Our objective function (that calls the weighed version of calc_sse())."""
            a = params[0]
            b = params[1]
            y_hat = linear_model(a, x, b)
            sse = calc_sse_weighed(y_hat, y, se)
            return sse

        starting_values = (0.0, 0.0)

        res_reps_weighed = optimize.fmin(
            objf_weighed, starting_values, args=(x, yy_mean, se)
        )

        self.assertAlmostEqual(np.min(res_reps_weighed[0]), 2.99095, places=5)
        self.assertAlmostEqual(np.min(res_reps_weighed[1]), 10.01920, places=5)

    def test_sse_2d(self):
        a_real = 3.0
        b_real = 10.0
        noise_level = 1.0

        start = -10.0
        stop = 10.0
        num = 101

        rng = np.random.RandomState(1)

        # Number of points
        x = np.linspace(start=start, stop=stop, num=num)
        y = a_real * x + b_real + noise_level * rng.randn(len(x))

        slopes = np.linspace(start=a_real - 25, stop=a_real + 25, num=51)
        intercepts = np.linspace(start=b_real - 25, stop=b_real + 25, num=51)
        SSEs = np.zeros((len(slopes), len(intercepts)))
        for i, a in enumerate(slopes):
            for j, b in enumerate(intercepts):
                y_hat = linear_model(a, x, b)
                SSEs[i, j] = calc_sse(y, y_hat)

        a_best, b_best = np.where(SSEs == np.min(SSEs))
        a = slopes[a_best[0]]
        b = intercepts[b_best[0]]

        self.assertEqual(a, 3.0)
        self.assertEqual(b, 10.0)
        self.assertAlmostEqual(np.min(SSEs), 78.91710, places=5)

        if platform.system() != "Linux":
            assert True
            return

        with TemporaryDirectory() as tmp_dir:
            plot_parameter_search_2d(
                slopes,
                intercepts,
                SSEs,
                axis_angle=(0.0, 0.0),
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_sse_2d_fig_01.tif",
            )
            img = imread(Path(tmp_dir) / "test_sse_2d_fig_01.tif")
        exp_img = imread(Path(__file__).parent / "data" / "exp_test_sse_2d_fig_01.tif")
        self.assertTrue(np.all(img == exp_img))

        # Close all figures
        plt.close("all")

    def testPlottingUnequalArrays(self):
        if platform.system() != "Linux":
            assert True
            return

        rng = np.random.RandomState(1)

        a_real = 3.0
        b_real = 10.0
        nReps = 5
        w0 = 3
        w1 = 0.5
        w2 = 10
        dataShift = 10

        x = np.linspace(start=0, stop=20, num=21)

        n1 = len(x) // 2
        n2 = len(x) - n1

        yy = np.zeros((nReps, len(x)))
        for i in range(nReps):
            noise = np.zeros(len(x))
            noise[:n1] = w1 * rng.randn(n1)
            noise[n1:] = dataShift + w2 * rng.randn(n2)
            yy[i, :] = a_real * x + b_real + w0 * noise

        se = yy.std(axis=0, ddof=1) / np.sqrt(nReps)

        with TemporaryDirectory() as tmp_dir:
            plot_data(
                x,
                yy,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_01.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_01.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_01.tif"
            )
            self.assertTrue(np.all(img == exp_img))

            plot_data(
                x,
                yy,
                lim_y=(-14, None),
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_02.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_02.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_02.tif"
            )
            self.assertTrue(np.all(img == exp_img))

            plot_data(
                x,
                yy,
                errors=se,
                errors_label="Standard error",
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_03.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_03.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_03.tif"
            )
            self.assertTrue(np.all(img == exp_img))

            plot_data(
                x,
                yy,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_04.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_04.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_04.tif"
            )
            self.assertTrue(np.all(img == exp_img))

            plot_data(
                x,
                yy,
                split_series=False,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_05.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_05.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_05.tif"
            )
            self.assertTrue(np.all(img == exp_img))

            plot_data(
                x,
                yy,
                errors=se,
                errors_label="Standard error",
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_06.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_06.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_06.tif"
            )
            self.assertTrue(np.all(img == exp_img))

            plot_data(
                x,
                yy,
                errors=se,
                errors_label="Standard error",
                split_series=False,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_07.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_07.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_07.tif"
            )
            self.assertTrue(np.all(img == exp_img))

        xx = np.repeat(np.reshape(x, (1, len(x))), nReps, axis=0)

        with TemporaryDirectory() as tmp_dir:
            plot_data(
                xx,
                yy,
                errors=se,
                errors_label="Standard error",
                split_series=False,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_08.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_08.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_08.tif"
            )
            self.assertTrue(np.all(img == exp_img))

            plot_data(
                xx,
                yy,
                errors=se,
                errors_label="Standard error",
                split_series=True,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_uneq_array_fig_09.tif",
            )
            img = imread(Path(tmp_dir) / "test_uneq_array_fig_09.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_uneq_array_fig_09.tif"
            )
            self.assertTrue(np.all(img == exp_img))

        self.assertEqual(len(x), 21)
        self.assertEqual(yy.shape, (5, 21))

        # Close all figures
        plt.close("all")

    def testPlottingExponential(self):
        x = np.arange(0, 10)
        y = np.array(
            [1708.0, 754.75, 455.5, 224.0, 109.0, 57.25, 31.75, 18.0, 6.0, 5.75]
        )
        a = 1686.9842
        b = -0.7105
        y_hat = np.array(
            [
                1686.98420937,
                829.01371777,
                407.39192485,
                200.19955868,
                98.38158503,
                48.3464416,
                23.75829191,
                11.6752426,
                5.7374196,
                2.81946893,
            ]
        )

        with TemporaryDirectory() as tmp_dir:
            plot_data(
                x=x,
                y=y,
                y_hat=y_hat,
                a=a,
                b=b,
                model_for_legend="exp",
                legend_location="best",
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_exp_fig_01.tif",
            )
            img = imread(Path(tmp_dir) / "test_exp_fig_01.tif")
            exp_img = imread(Path(__file__).parent / "data" / "exp_test_exp_fig_01.tif")
            self.assertTrue(np.all(img == exp_img))

        self.assertEqual(True, True)

        # Close all figures
        plt.close("all")

    def testPlottingTwoFits(self):
        if platform.system() != "Linux":
            assert True
            return

        rng = np.random.RandomState(1)

        a_real = 3.0
        b_real = 10.0
        nReps = 5  # Number of repeated (independent) measurements y.
        w0 = 3.0  # Noise level on the repeated measurement example.
        w1 = 0.5  # Weight that scales the standard deviation of the noise for the first half of the data.
        w2 = 5.0  # Weight that scales the standard deviation of the noise for the second half of the data.
        data_offset = 5.0  # Amount by which the second half of the data is moved away from the ideal fit.

        x = np.linspace(start=0, stop=20, num=21)

        n1 = len(x) // 2
        n2 = len(x) - n1

        yy = np.zeros((nReps, len(x)))
        for i in range(nReps):
            noise = np.zeros(len(x))
            noise[:n1] = w1 * rng.randn(n1)
            noise[n1:] = data_offset + w2 * rng.randn(n2)
            yy[i, :] = a_real * x + b_real + w0 * noise

        xx = np.repeat(np.reshape(x, (1, len(x))), nReps, axis=0)

        def calc_sse(y, y_hat):
            """Calculate the Sum of Squared Errors between prediction y_hat and data y."""
            sse = np.sum(np.power(y_hat - y, 2))
            return sse

        def linear_model(x, a, b):
            """Our linear model y = a * x + b."""
            y_hat = a * x + b
            return y_hat

        def objf(params, x, y):
            """Our objective function."""
            a = params[0]
            b = params[1]
            y_hat = linear_model(x, a, b)
            sse = calc_sse(y_hat, y)
            return sse

        # Starting values for the optimization (initial values for a and b)
        starting_values = (0.0, 0.0)

        # Run the optimization
        res_reps = optimize.fmin(objf, starting_values, args=(xx, yy))
        a_est_reps = res_reps[0]
        b_est_reps = res_reps[1]

        y_hat = linear_model(x, a_est_reps, b_est_reps)

        yy_mean = yy.mean(axis=0)
        sd = yy.std(axis=0, ddof=1)
        se = sd / np.sqrt(nReps)

        def calc_sse_weighed(y, y_hat, se):
            """Calculate the Sum of Squared Errors between prediction y_hat and data y."""
            sse = np.sum(np.power(y_hat - y, 2) / np.power(se, 2))
            return sse

        def linear_model(x, a, b):
            """Our linear model y = a * x + b."""
            y_hat = a * x + b
            return y_hat

        def objf_weighed(params, x, y, se):
            """Our objective function (that calls the weighed version of calc_sse())."""
            a = params[0]
            b = params[1]
            y_hat = linear_model(x, a, b)
            sse = calc_sse_weighed(y_hat, y, se)
            return sse

        # Starting values for the optimization (initial values for a and b)
        starting_values = (0.0, 0.0)

        # Run the optimization
        res_reps_weighed = optimize.fmin(
            objf_weighed, starting_values, args=(x, yy_mean, se)
        )

        a_est_weighed = res_reps_weighed[0]
        b_est_weighed = res_reps_weighed[1]
        y_hat_weighted = linear_model(x, a_est_weighed, b_est_weighed)

        with TemporaryDirectory() as tmp_dir:
            fig, ax = plot_data(
                xx,
                yy,
                x2=x,
                y_hat=y_hat_weighted,
                a=a_est_weighed,
                b=b_est_weighed,
                errors=se,
                errors_label="Standard error",
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_two_fits_fig_01.tif",
            )
            img = imread(Path(tmp_dir) / "test_two_fits_fig_01.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_two_fits_fig_01.tif"
            )
            self.assertTrue(np.all(img == exp_img))

            add_fit(
                fig,
                ax,
                x=x,
                y_hat=y_hat,
                a=a_est_reps,
                b=b_est_reps,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_two_fits_fig_02.tif",
            )
            img = imread(Path(tmp_dir) / "test_two_fits_fig_02.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_two_fits_fig_02.tif"
            )
            self.assertTrue(np.all(img == exp_img))

        # Close all figures
        plt.close("all")

    def testShowLabels(self):
        # Read image
        bw = imread(str(Path(__file__).parent / "data" / "test_watershed_size_bw.tif"))

        # Extract objects from bw mask
        labels, num = label(bw, background=0, return_num=True, connectivity=1)

        with TemporaryDirectory() as tmp_dir:
            fig, (ax1, ax2) = plt.subplots(1, 2, dpi=150)
            show_labels(labels, ax=ax1, title="256 colors")
            show_labels(
                labels, cmap=get_labels_cmap(65536), ax=ax2, title="65536 colors"
            )
            fig.savefig(Path(tmp_dir) / "test_show_labels_fig_01.tif")
            img = imread(Path(tmp_dir) / "test_show_labels_fig_01.tif")
            exp_img = imread(
                Path(__file__).parent / "data" / "exp_test_show_labels_fig_01.tif"
            )
            self.assertTrue(np.all(img == exp_img))

        # Close all figures
        plt.close("all")

    def testModelsForLegend(self):
        x = np.arange(0, 10)
        y = x + np.random.rand(10)
        y_hat = np.arange(0, 10)

        plot_data(x, y, y_hat=y_hat, a=1, b=1, model_for_legend="linear")
        plot_data(x, y, y_hat=y_hat, a=1, b=1, model_for_legend="exp")
        plot_data(x, y, y_hat=y_hat, a=1, b=1, model_for_legend="log")
        plot_data(x, y, y_hat=y_hat, a=1, b=1, model_for_legend="sqrt")

        # Close all figures
        plt.close("all")

    def testArgumentShapeCombinations(self):
        x = np.arange(10).reshape(1, 10)
        xx = np.repeat(x, 4, axis=0)
        y = np.arange(10).reshape(1, 10)
        yy = np.repeat(y, 4, axis=0)
        y_hat = np.arange(10)
        se_1 = np.arange(10).reshape(1, 10)
        se_2 = np.repeat(se_1, 4, axis=0)

        plot_data(x, y, y_hat=y_hat)
        plot_data(x, yy, y_hat=y_hat)
        plot_data(xx, yy, y_hat=y_hat)
        plot_data(xx, yy, x2=x, y_hat=y_hat)
        with self.assertRaises(ValueError):
            plot_data(xx, yy, x2=xx, y_hat=y_hat)
        with self.assertRaises(ValueError):
            plot_data(xx, yy, x2=xx, y_hat=yy)
        plot_data(xx, yy, x2=x, y_hat=y_hat, errors=se_1)
        with self.assertRaises(ValueError):
            plot_data(xx, yy, x2=x, y_hat=y_hat, errors=se_2)

        # Close all figures
        plt.close("all")


if __name__ == "__main__":
    unittest.main()
