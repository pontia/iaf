import time
import unittest
from pathlib import Path
from tempfile import TemporaryDirectory

import matplotlib.pyplot as plt
import numpy as np
from tifffile import imread

from iaf.plot.validation import plot_background_subtraction_control
from iaf.process import sample, subtract_background, tile


class TestFilters(unittest.TestCase):
    def test_background_subtraction(self):
        # Read expected profiles
        data = np.load(
            str(Path(__file__).parent / "data" / "bkgd_subtr.npz"), allow_pickle=True
        )

        # Read image to process
        img = imread(str(Path(__file__).parent / "data" / "cells_shaded.tif"))

        # Subtract background with the rolling ball algorithm
        img_corr, background = subtract_background(
            img, algorithm="rolling_ball", radius=15, return_background=True
        )

        # Get the profiles
        (
            p_img_x,
            p_img_y,
            p_r_corr_x,
            p_r_corr_y,
            p_r_bckg_x,
            p_r_bckg_y,
        ) = self._get_profiles(img, img_corr, background)

        with TemporaryDirectory() as tmp_dir:
            plot_background_subtraction_control(
                img,
                background,
                img_corr,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_bkg_subtr_fig_01.tif",
            )
            test_img = imread(str(Path(tmp_dir) / "test_bkg_subtr_fig_01.tif"))
            exp_test_img = imread(
                str(Path(__file__).parent / "data" / "exp_test_bkg_subtr_fig_01.tif")
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Check the results
        self.assertTrue(np.all(p_r_corr_x == data["exp_p_r_corr_x"]))
        self.assertTrue(np.all(p_r_corr_y == data["exp_p_r_corr_y"]))
        self.assertTrue(np.all(p_r_bckg_x == data["exp_p_r_bckg_x"]))
        self.assertTrue(np.all(p_r_bckg_y == data["exp_p_r_bckg_y"]))

        # Subtract background with the morphological opening algorithm
        img_corr, background = subtract_background(
            img, algorithm="morphological_opening", radius=15, return_background=True
        )

        with TemporaryDirectory() as tmp_dir:
            plot_background_subtraction_control(
                img,
                background,
                img_corr,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_bkg_subtr_fig_02.tif",
            )
            test_img = imread(str(Path(tmp_dir) / "test_bkg_subtr_fig_02.tif"))
            exp_test_img = imread(
                str(Path(__file__).parent / "data" / "exp_test_bkg_subtr_fig_02.tif")
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Get the profiles
        _, _, p_m_corr_x, p_m_corr_y, p_m_bckg_x, p_m_bckg_y = self._get_profiles(
            img, img_corr, background
        )

        # Check the results
        self.assertTrue(np.all(p_m_corr_x == data["exp_p_m_corr_x"]))
        self.assertTrue(np.all(p_m_corr_y == data["exp_p_m_corr_y"]))
        self.assertTrue(np.all(p_m_bckg_x == data["exp_p_m_bckg_x"]))
        self.assertTrue(np.all(p_m_bckg_y == data["exp_p_m_bckg_y"]))

        # Subtract background with the gaussian  algorithm
        img_corr, background = subtract_background(
            img, algorithm="gaussian", radius=15, return_background=True
        )

        with TemporaryDirectory() as tmp_dir:
            plot_background_subtraction_control(
                img,
                background,
                img_corr,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_bkg_subtr_fig_03.tif",
            )
            test_img = imread(str(Path(tmp_dir) / "test_bkg_subtr_fig_03.tif"))
            exp_test_img = imread(
                str(Path(__file__).parent / "data" / "exp_test_bkg_subtr_fig_03.tif")
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Get the profiles
        _, _, p_g_corr_x, p_g_corr_y, p_g_bckg_x, p_g_bckg_y = self._get_profiles(
            img, img_corr, background
        )

        # Check the results
        self.assertTrue(np.all(p_g_corr_x == data["exp_p_g_corr_x"]))
        self.assertTrue(np.all(p_g_corr_y == data["exp_p_g_corr_y"]))
        self.assertTrue(np.all(p_g_bckg_x == data["exp_p_g_bckg_x"]))
        self.assertTrue(np.all(p_g_bckg_y == data["exp_p_g_bckg_y"]))

        # Close all figures
        plt.close("all")

        # # Use this if you need to update the expected results
        # with open('bkgd_subtr.npz', 'wb') as f:
        #     np.savez(f,
        #              exp_p_r_corr_x=p_r_corr_x,
        #              exp_p_r_corr_y=p_r_corr_y,
        #              exp_p_r_bckg_x=p_r_bckg_x,
        #              exp_p_r_bckg_y=p_r_bckg_y,
        #              exp_p_m_corr_x=p_m_corr_x,
        #              exp_p_m_corr_y=p_m_corr_y,
        #              exp_p_m_bckg_x=p_m_bckg_x,
        #              exp_p_m_bckg_y=p_m_bckg_y,
        #              exp_p_g_corr_x=p_g_corr_x,
        #              exp_p_g_corr_y=p_g_corr_y,
        #              exp_p_g_bckg_x=p_g_bckg_x,
        #              exp_p_g_bckg_y=p_g_bckg_y
        #     )

    def _get_profiles(self, img, img_corr, background):
        x = img.shape[1] // 2
        y = img.shape[0] // 2

        profile_img_x = img[y, :]
        profile_corr_x = img_corr[y, :]
        profile_background_x = background[y, :]

        profile_img_y = img[:, x]
        profile_corr_y = img_corr[:, x]
        profile_background_y = background[:, x]

        return (
            profile_img_x,
            profile_img_y,
            profile_corr_x,
            profile_corr_y,
            profile_background_x,
            profile_background_y,
        )

    def test_background_subtraction_scales(self):
        # Read image to process
        img = imread(str(Path(__file__).parent / "data" / "cells_shaded.tif"))

        t0 = time.time()
        corr_1, back_1 = subtract_background(
            img,
            algorithm="morphological_opening",
            radius=25,
            offset=0,
            down_size_factor=1,
            return_background=True,
        )
        print(f"Elapsed time = {time.time() - t0} s")
        self.assertTrue(np.all(np.array(img.shape) == np.array(corr_1.shape)))
        self.assertTrue(np.all(np.array(img.shape) == np.array(back_1.shape)))

        with TemporaryDirectory() as tmp_dir:
            plot_background_subtraction_control(
                img,
                back_1,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_bkg_subtr_scales_fig_01.tif",
            )
            test_img = imread(str(Path(tmp_dir) / "test_bkg_subtr_scales_fig_01.tif"))
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_bkg_subtr_scales_fig_01.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        t0 = time.time()
        corr_2, back_2 = subtract_background(
            img,
            algorithm="morphological_opening",
            radius=25,
            offset=0,
            down_size_factor=2,
            return_background=True,
        )
        print(f"Elapsed time = {time.time() - t0} s")
        self.assertTrue(np.all(np.array(img.shape) == np.array(corr_2.shape)))
        self.assertTrue(np.all(np.array(img.shape) == np.array(back_2.shape)))

        with TemporaryDirectory() as tmp_dir:
            plot_background_subtraction_control(
                img,
                back_2,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_bkg_subtr_scales_fig_02.tif",
            )
            test_img = imread(str(Path(tmp_dir) / "test_bkg_subtr_scales_fig_02.tif"))
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_bkg_subtr_scales_fig_02.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        t0 = time.time()
        corr_3, back_3 = subtract_background(
            img,
            algorithm="morphological_opening",
            radius=25,
            offset=0,
            down_size_factor=4,
            return_background=True,
        )
        print(f"Elapsed time = {time.time() - t0} s")
        self.assertTrue(np.all(np.array(img.shape) == np.array(corr_3.shape)))
        self.assertTrue(np.all(np.array(img.shape) == np.array(back_3.shape)))

        with TemporaryDirectory() as tmp_dir:
            plot_background_subtraction_control(
                img,
                back_3,
                dpi=150,
                out_file_name=Path(tmp_dir) / "test_bkg_subtr_scales_fig_04.tif",
            )
            test_img = imread(str(Path(tmp_dir) / "test_bkg_subtr_scales_fig_04.tif"))
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_bkg_subtr_scales_fig_04.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

            fig, (ax1, ax2, ax3) = plt.subplots(figsize=(20, 6), dpi=150, ncols=3)
            p1 = ax1.imshow(corr_1, cmap="jet", interpolation="none")
            p2 = ax2.imshow(
                corr_1.astype(np.float32) - corr_2.astype(np.float32),
                cmap="jet",
                interpolation="none",
            )
            p3 = ax3.imshow(
                corr_1.astype(np.float32) - corr_3.astype(np.float32),
                cmap="jet",
                interpolation="none",
            )
            fig.colorbar(p1, ax=ax1)
            fig.colorbar(p2, ax=ax2)
            fig.colorbar(p3, ax=ax3)
            plt.savefig(
                str(Path(tmp_dir) / "test_bkg_subtr_scales_fig_05.tif"), dpi=150
            )
            test_img = imread(str(Path(tmp_dir) / "test_bkg_subtr_scales_fig_05.tif"))
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_bkg_subtr_scales_fig_05.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Close all figures
        plt.close("all")

    def testSample(self):
        image = np.arange(10000).reshape((100, 100))

        subset_1 = sample(image, (10, 10), seed=2022)
        subset_2 = sample(image, (10, 10), seed=2022)
        subset_3 = sample(image, (10, 10), seed=2021)
        subset_4 = sample(image, (10, 10))

        self.assertTrue(np.all(subset_1 == subset_2))
        self.assertFalse(np.all(subset_1 == subset_3))
        self.assertFalse(np.all(subset_1 == subset_4))
        self.assertEqual(subset_1.shape, (10, 10))

    def testTile(self):
        image = np.arange(10000).reshape((100, 100))

        tiles, n_rows, n_cols = tile(image, (25, 25))
        self.assertEqual(len(tiles), 16)
        self.assertEqual(tiles[0].shape, (25, 25))
        self.assertEqual(n_rows, 4)
        self.assertEqual(n_cols, 4)

        tiles, n_rows, n_cols = tile(image, (50, 25))
        self.assertEqual(len(tiles), 8)
        self.assertEqual(tiles[0].shape, (50, 25))
        self.assertEqual(n_rows, 2)
        self.assertEqual(n_cols, 4)

        tiles, n_rows, n_cols = tile(image, (25, 50))
        self.assertEqual(len(tiles), 8)
        self.assertEqual(tiles[0].shape, (25, 50))
        self.assertEqual(n_rows, 4)
        self.assertEqual(n_cols, 2)

        tiles, n_rows, n_cols = tile(image, (100, 100))
        self.assertEqual(len(tiles), 1)
        self.assertEqual(tiles[0].shape, (100, 100))
        self.assertEqual(n_rows, 1)
        self.assertEqual(n_cols, 1)

        tiles, n_rows, n_cols = tile(image, (50, 50), overlap_pixels=10)
        self.assertEqual(len(tiles), 4)
        self.assertEqual(tiles[0].shape, (50, 50))
        self.assertEqual(n_rows, 2)
        self.assertEqual(n_cols, 2)

        tiles, n_rows, n_cols = tile(
            image, (50, 50), overlap_pixels=10, drop_partial=False
        )
        self.assertEqual(len(tiles), 9)
        self.assertEqual(tiles[0].shape, (50, 50))
        self.assertEqual(n_rows, 3)
        self.assertEqual(n_cols, 3)
        self.assertEqual(tiles[-1].shape, (20, 20))

        tiles, n_rows, n_cols = tile(
            image, (50, 50), overlap_percent=0.20, drop_partial=False
        )
        self.assertEqual(len(tiles), 9)
        self.assertEqual(tiles[0].shape, (50, 50))
        self.assertEqual(n_rows, 3)
        self.assertEqual(n_cols, 3)
        self.assertEqual(tiles[-1].shape, (20, 20))


if __name__ == "__main__":
    unittest.main()
