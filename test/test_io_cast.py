import unittest

import numpy as np

from iaf.io.cast import safe_to_uint8, safe_to_uint16


class TestIOCast(unittest.TestCase):
    def testSafeTypeConversions(self):
        # To uint 8
        img = np.array([[100, 100], [100, 100]], dtype=np.uint16)
        exp_out = np.array([[100, 100], [100, 100]], dtype=np.uint8)
        out = safe_to_uint8(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        img = np.array([[100, 100], [100, 300]], dtype=np.uint16)
        exp_out = np.array([[100, 100], [100, 255]], dtype=np.uint8)
        out = safe_to_uint8(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))
        self.assertTrue(img[1, 1] == 300)  # No modification to the input array!

        img = np.array([[True, True], [True, True]], dtype=bool)
        exp_out = np.array([[1, 1], [1, 1]], dtype=np.uint8)
        out = safe_to_uint8(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        img = np.array([[False, False], [False, False]], dtype=bool)
        exp_out = np.array([[0, 0], [0, 0]], dtype=np.uint8)
        out = safe_to_uint8(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        img = np.array([[100.0, 100.0], [100.0, 100.0]], dtype=np.float32)
        exp_out = np.array([[100, 100], [100, 100]], dtype=np.uint8)
        out = safe_to_uint8(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        img = np.array([[100.0, 100.0], [100.0, 300.0]], dtype=np.float32)
        exp_out = np.array([[100, 100], [100, 255]], dtype=np.uint8)
        out = safe_to_uint8(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))
        self.assertTrue(img[1, 1] == 300.0)  # No modification to the input array!

        img = np.array([[100.0, 100.0], [100.0, 300.0]], dtype=float)  # np.float64
        exp_out = np.array([[100, 100], [100, 255]], dtype=np.uint8)
        out = safe_to_uint8(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        # To uint 16
        img = np.array([[100, 100], [100, 100]], dtype=np.uint16)
        exp_out = np.array([[100, 100], [100, 100]], dtype=np.uint16)
        out = safe_to_uint16(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        img = np.array([[100, 100], [100, 300]], dtype=np.uint16)
        exp_out = np.array([[100, 100], [100, 300]], dtype=np.uint16)
        out = safe_to_uint16(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        img = np.array([[True, True], [True, True]], dtype=bool)
        exp_out = np.array([[1, 1], [1, 1]], dtype=np.uint16)
        out = safe_to_uint16(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        img = np.array([[False, False], [False, False]], dtype=bool)
        exp_out = np.array([[0, 0], [0, 0]], dtype=np.uint16)
        out = safe_to_uint16(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))

        img = np.array([[100.0, 100.0], [100.0, 300000.0]], dtype=np.float32)
        exp_out = np.array([[100, 100], [100, 65535]], dtype=np.uint16)
        out = safe_to_uint16(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))
        self.assertTrue(img[1, 1] == 300000.0)  # No modification to the input array!

        img = np.array([[100.0, 100.0], [100.0, 300000.0]], dtype=float)  # np.float64
        exp_out = np.array([[100, 100], [100, 65535]], dtype=np.uint16)
        out = safe_to_uint16(img)
        self.assertTrue(out.dtype == exp_out.dtype)
        self.assertTrue(np.all(out == exp_out))
        self.assertTrue(img[1, 1] == 300000.0)  # No modification to the input array!


if __name__ == "__main__":
    unittest.main()
