import unittest
import warnings
from pathlib import Path
from tempfile import TemporaryDirectory

import matplotlib.pyplot as plt
import numpy as np
from skimage.io import imread, imsave
from skimage.measure import label, regionprops

import iaf.plot
from iaf.color import get_labels_cmap
from iaf.morph.watershed import (
    estimate_object_sizes,
    filter_labels_by_area,
    get_label_areas,
    separate_neighboring_objects,
)


class TestWatershed(unittest.TestCase):
    def test_separate_neighboring_objects_params(self):
        bw = imread(Path(__file__).parent / "data" / "bw.png")

        # Get some object size estimate
        area, min_axis, max_axis, equiv_diam = estimate_object_sizes(bw)

        # Set filter and maximum suppression size
        filter_size = int((min_axis * max_axis) ** 0.5)
        maxima_suppression_size = filter_size

        # Count objects
        labels, num = label(bw, background=0, return_num=True, connectivity=1)

        # Test
        self.assertEqual(num, 95)

        # Get area information
        props = regionprops(labels)

        areas = []
        for prop in props:
            areas.append(prop.area)
        areas = np.array(areas)

        # Test
        self.assertEqual(areas.min(), 61)
        self.assertAlmostEqual(areas.mean(), 185.0631, 2)
        self.assertEqual(areas.max(), 404)

        # Now run a watershed
        labels_ws, num_ws, max_suppress = separate_neighboring_objects(bw, labels)

        # Test
        self.assertEqual(num_ws, 97)

        with TemporaryDirectory() as tmp_dir:
            plt.imshow(
                labels_ws,
                cmap=iaf.color._color.get_labels_cmap(),
                interpolation="nearest",
            )
            plt.savefig(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_01.tif"), dpi=150
            )
            test_img = imread(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_01.tif")
            )
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_sep_neigh_objs_params_fig_01.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Get area information
        props_ws = regionprops(labels_ws)

        areas_ws = []
        for prop in props_ws:
            areas_ws.append(prop.area)
        areas_ws = np.array(areas_ws)

        # Test
        self.assertEqual(areas_ws.min(), 61)
        self.assertAlmostEqual(areas_ws.mean(), 181.247, 2)
        self.assertEqual(areas_ws.max(), 236)

        # More argument tests
        labels_f, num_ws_f, max_suppress_f = separate_neighboring_objects(
            bw,
            labels,
            filter_size=filter_size,
            watershed_method="shape",
            unclump_method="shape",
            low_res_maxima=True,
        )

        with TemporaryDirectory() as tmp_dir:
            plt.imshow(
                labels_f,
                cmap=get_labels_cmap(),
                interpolation="nearest",
            )
            plt.savefig(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_02.tif"), dpi=150
            )
            test_img = imread(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_02.tif")
            )
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_sep_neigh_objs_params_fig_02.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Test
        self.assertEqual(num_ws_f, 97)

        labels_m, num_ws_m, max_suppress_m = separate_neighboring_objects(
            bw,
            labels,
            maxima_suppression_size=maxima_suppression_size,
            watershed_method="shape",
            unclump_method="shape",
            low_res_maxima=True,
        )

        with TemporaryDirectory() as tmp_dir:
            plt.imshow(
                labels_m,
                cmap=get_labels_cmap(),
                interpolation="nearest",
            )
            plt.savefig(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_03.tif"), dpi=150
            )
            test_img = imread(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_03.tif")
            )
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_sep_neigh_objs_params_fig_03.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Test
        self.assertEqual(num_ws_m, 97)

        labels_f_m, num_ws_f_m, max_suppress_f_m = separate_neighboring_objects(
            bw,
            labels,
            filter_size=filter_size,
            maxima_suppression_size=maxima_suppression_size,
            watershed_method="shape",
            unclump_method="shape",
            low_res_maxima=True,
        )

        with TemporaryDirectory() as tmp_dir:
            plt.imshow(
                labels_f_m,
                cmap=get_labels_cmap(),
                interpolation="nearest",
            )
            plt.savefig(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_04.tif"), dpi=150
            )
            test_img = imread(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_04.tif")
            )
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_sep_neigh_objs_params_fig_04.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Test
        self.assertEqual(num_ws_f_m, 97)

        labels_s, num_ws_s, max_suppress_f_m = separate_neighboring_objects(
            bw,
            labels,
            min_size=25,
            watershed_method="shape",
            unclump_method="shape",
            low_res_maxima=True,
        )

        with TemporaryDirectory() as tmp_dir:
            plt.imshow(
                labels_s,
                cmap=get_labels_cmap(),
                interpolation="nearest",
            )
            plt.savefig(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_05.tif"), dpi=150
            )
            test_img = imread(
                str(Path(tmp_dir) / "test_sep_neigh_objs_params_fig_05.tif")
            )
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_sep_neigh_objs_params_fig_05.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Test
        self.assertEqual(num_ws_s, 97)

        # Close all figures
        plt.close("all")

    def test_separate_neighboring_objects(self):
        bw = imread(str(Path(__file__).parent / "data" / "test_watershed_size_bw.tif"))

        # Set filter and maxima suppression sizes
        filter_size = 13
        maxima_suppression_size = 14
        min_size = 20

        # Extract objects from bw mask
        labels, num = label(bw, background=0, return_num=True, connectivity=1)

        self.assertEqual(num, 28)

        # Let the algorithm decide based on min size
        labels_auto, num_auto, _ = separate_neighboring_objects(
            bw, labels, min_size=min_size
        )

        self.assertEqual(num_auto, 31)

        with TemporaryDirectory() as tmp_dir:
            plt.imshow(
                labels_auto,
                cmap=get_labels_cmap(),
                interpolation="nearest",
            )
            plt.savefig(str(Path(tmp_dir) / "test_sep_neigh_objs_fig_01.tif"), dpi=150)
            test_img = imread(str(Path(tmp_dir) / "test_sep_neigh_objs_fig_01.tif"))
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_sep_neigh_objs_fig_01.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        labels_ws, num_ws, _ = separate_neighboring_objects(
            bw,
            labels,
            filter_size=filter_size,
            maxima_suppression_size=maxima_suppression_size,
        )

        self.assertEqual(num_ws, 31)

        with TemporaryDirectory() as tmp_dir:
            plt.imshow(
                labels_ws,
                cmap=get_labels_cmap(),
                interpolation="nearest",
            )
            plt.savefig(str(Path(tmp_dir) / "test_sep_neigh_objs_fig_02.tif"), dpi=150)
            test_img = imread(str(Path(tmp_dir) / "test_sep_neigh_objs_fig_02.tif"))
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_sep_neigh_objs_fig_02.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        labels_ws_b, num_ws_b, _ = separate_neighboring_objects(
            bw,
            labels,
            filter_size=filter_size,
            maxima_suppression_size=maxima_suppression_size,
            exclude_border_objects=True,
        )

        self.assertEqual(num_ws_b, 27)

        with TemporaryDirectory() as tmp_dir:
            plt.imshow(
                labels_ws_b,
                cmap=get_labels_cmap(),
                interpolation="nearest",
            )
            plt.savefig(str(Path(tmp_dir) / "test_sep_neigh_objs_fig_03.tif"), dpi=150)
            test_img = imread(str(Path(tmp_dir) / "test_sep_neigh_objs_fig_03.tif"))
            exp_test_img = imread(
                str(
                    Path(__file__).parent
                    / "data"
                    / "exp_test_sep_neigh_objs_fig_03.tif"
                )
            )
            self.assertTrue(np.all(test_img == exp_test_img))

        # Close all figures
        plt.close("all")

    def test_removing_objects_by_size(self):
        bw = imread(Path(__file__).parent / "data" / "bw.png")

        # Count objects
        labels, num = label(bw, background=0, return_num=True, connectivity=1)

        # Test
        self.assertEqual(num, 95)

        # Get area information
        props = regionprops(labels)

        areas = []
        for prop in props:
            areas.append(prop.area)
        areas = np.array(areas)

        areas_from_method, _, _ = get_label_areas(labels)
        self.assertTrue(np.all(areas == areas_from_method))

        # Test
        self.assertEqual(areas.min(), 61)
        self.assertAlmostEqual(areas.mean(), 185.0631, 2)
        self.assertEqual(areas.max(), 404)

        # Now run a watershed
        labels_ws, num_ws, max_suppress = separate_neighboring_objects(
            bw, labels, min_size=20, max_size=100
        )

        # Test
        self.assertEqual(num_ws, 97)

        # Remove all objects from the original label image that have 100 < areas < 300
        labels_filt, num_labels_filt = filter_labels_by_area(
            labels, min_area=100, max_area=300
        )

        self.assertEqual(num_labels_filt, len(np.unique(labels_filt)) - 1)

        # Test
        self.assertEqual(num_labels_filt, 88)

        # Re-measure
        props = regionprops(labels_filt)

        areas = []
        for prop in props:
            areas.append(prop.area)
        areas = np.array(areas)

        areas_from_method, _, _ = get_label_areas(labels_filt)
        self.assertTrue(np.all(areas == areas_from_method))

        # Test
        self.assertTrue(np.all(areas > 100))
        self.assertTrue(np.all(areas < 300))

        with TemporaryDirectory() as tmp_dir:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                imsave(
                    str(Path(tmp_dir) / "test_test_filt_by_size_01.tif"), labels_filt
                )
            test_img = imread(str(Path(tmp_dir) / "test_test_filt_by_size_01.tif"))
            exp_test_img = imread(
                str(Path(__file__).parent / "data" / "exp_test_filt_by_size_01.tif")
            )
            self.assertTrue(np.all((test_img > 0) == (exp_test_img > 0)))

        # Background is biggest
        a = np.zeros((10, 10), dtype=np.uint8)
        a[2, 2:4] = 1  # Area = 2
        a[2:5, 6:9] = 2  # Area = 9
        a[5:9, 1:5] = 3  # Area = 16

        # Remove all objects from the original label image that have areas > 10
        labels_filt_1, num_labels_filt_1 = filter_labels_by_area(a, min_area=10)
        exp_labels_filt_1 = np.zeros((10, 10), dtype=np.uint8)
        exp_labels_filt_1[5:9, 1:5] = 3  # Area = 16
        self.assertTrue(np.all((labels_filt_1 > 0) == (exp_labels_filt_1 > 0)))
        self.assertTrue(num_labels_filt_1 == 1)

        # Remove all objects from the original label image that have 3 < areas < 10
        labels_filt_2, num_labels_filt_2 = filter_labels_by_area(
            a, min_area=3, max_area=10
        )
        exp_labels_filt_2 = np.zeros((10, 10), dtype=np.uint8)
        exp_labels_filt_2[2:5, 6:9] = 2  # Area = 9
        self.assertTrue(np.all((labels_filt_2 > 0) == (exp_labels_filt_2 > 0)))
        self.assertTrue(num_labels_filt_2 == 1)

        # Background is smaller than objects
        a = np.zeros((10, 10), dtype=np.uint8)
        a[1:9, 1:9] = 2  # Area = 64

        # Remove all objects from the original label image that have areas > 50
        labels_filt_3, num_labels_filt_3 = filter_labels_by_area(a, min_area=50)
        exp_labels_filt_3 = a.copy()
        self.assertTrue(np.all((labels_filt_3 > 0) == (exp_labels_filt_3 > 0)))
        self.assertTrue(num_labels_filt_3 == 1)

        # Remove all objects from the original label image that have areas < 20
        labels_filt_4, num_labels_filt_4 = filter_labels_by_area(
            a, min_area=0, max_area=20
        )
        exp_labels_filt_4 = np.zeros((10, 10), dtype=np.uint8)
        self.assertTrue(np.all((labels_filt_4 > 0) == (exp_labels_filt_4 > 0)))
        self.assertTrue(num_labels_filt_4 == 0)


if __name__ == "__main__":
    unittest.main()
